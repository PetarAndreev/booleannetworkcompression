import numpy as np

a = np.array([1, 0, -1, -1])
a_i = np.array([a[0], a[0], a[1], a[1]])

# print(a_i)

b = np.array([0, 1, -1, -1])
b_i = np.array([b[0], b[1], b[0], b[1]])

# print(b_i)


intermediate = np.mean([a_i, b_i], axis = 0)
# print(intermediate)

#####

def generate_combinations(intermediate):
    results = []
    generate_combinations_recursive(intermediate, 0, [], results)
    return np.array(results)

def generate_combinations_recursive(intermediate, index, current, results):
    if index == len(intermediate):
        results.append(np.array(current.copy()))
        return

    if intermediate[index] == 0 or intermediate[index] == 1:
        current.append(intermediate[index])
        generate_combinations_recursive(intermediate, index + 1, current, results)
        current.pop()
    elif intermediate[index] == 0.5:
        current.append(0)
        generate_combinations_recursive(intermediate, index + 1, current, results)
        current.pop()

        current.append(1)
        generate_combinations_recursive(intermediate, index + 1, current, results)
        current.pop()

# Примерно извикване

combinations = generate_combinations(intermediate)
combinations = combinations.astype(int)
print(combinations)

