import numpy as np
import json


def combineRows(matrix, row1, row2):
    assert(row1 != row2)
    if row1 > row2:
        row1, row2 = row2, row1
    [rows, columns] = matrix.shape
    newMatrix = np.full([rows-1, 2*columns], -1)

    for i in range(columns):
        newMatrix[0][i] = matrix[row1][i]
        newMatrix[0][i+columns] = matrix[row2][i]

    for r in range(1, row1+1):
        for c in range(columns):
            newMatrix[r][c] = matrix[r-1][c]

    for r in range(row1+1, row2):
        for c in range(columns):
            newMatrix[r][c] = matrix[r][c]

    for r in range(row2, rows-1):
        for c in range(columns):
            newMatrix[r][c] = matrix[r+1][c]

    return newMatrix


def fixIndices(matrix, row1, row2):
    [rows, columns] = matrix.shape
    for r in range(rows):
        for c in range(columns):
            if matrix[r][c] == row1 or matrix[r][c] == row2:
                matrix[r][c] = 0
            elif matrix[r][c] != -1 and matrix[r][c] < row1:
                matrix[r][c] += 1
            elif matrix[r][c] > row2:
                matrix[r][c] -= 1

def removeDuplicates(row):
    for i in range(len(row)-1):
        if row[i] != -1:
            for j in range(i+1, len(row)):
                if row[i] == row[j]:
                    row[j] = -1

def reorderRow(row):
    last = -1
    for i in range(len(row)):
        if row[i] != -1:
            last = i

    for i in range(last, 0, -1):
        if row[i] == -1:
            for j in range(i, last):
                row[j] = row[j+1]
            row[last] = -1
            last -= 1

def removeDuplicatesAndReorder(matrix):
    [rows, columns] = matrix.shape
    for r in range(rows):
        removeDuplicates(matrix[r])
        reorderRow(matrix[r])


# function to compare Hamming distance - of CP rank or attractor statesS 

def hamming2(s1, s2):
    """Calculate the Hamming distance between two bit strings"""
    assert len(s1) == len(s2)
    return sum(c1 != c2 for c1, c2 in zip(s1, s2))

# function to present only best fits when comparing attractor states between primary and compressed

def get_lowest_score_rows(arr):
    # Group the rows by the binary string in the first column
    groups = {}
    for row in arr:
        key = row[0]
        if key not in groups:
            groups[key] = []
        groups[key].append(row)
    
    # Find the row with the lowest score in the third column for each group
    lowest_score_rows = []
    for group in groups.values():
        lowest_score_row = min(group, key=lambda x: int(x[2]))
        lowest_score_rows.append(lowest_score_row)
    
    # Return the rows
    return np.array(lowest_score_rows)

### a function to create state transition diagram for the primary network

def export_to_json(net):
    attractors = net.get_attractors()
    edges = net.get_edges()

    d = [{"state":int(index),
    "level":int(attractors[index,1]),
    "attractor":int(attractors[index,0]),
    "target":int(edges[index])} for index in range(net.n_states)]
    
    return(json.dumps(d))