from boolean_networks import utils
from boolean_networks.gene_network import GeneNetwork
from boolean_networks import bn

import numpy as np
import time

import csv
import os

import Functions


start_time = time.time()

def generate_combinations(intermediate):
    results = []
    generate_combinations_recursive(intermediate, 0, [], results)
    return np.array(results)

def generate_combinations_recursive(intermediate, index, current, results):
    if index == len(intermediate):
        results.append(np.array(current.copy()))
        return

    if intermediate[index] == 0 or intermediate[index] == 1:
        current.append(intermediate[index])
        generate_combinations_recursive(intermediate, index + 1, current, results)
        current.pop()
    elif intermediate[index] == 0.5:
        current.append(0)
        generate_combinations_recursive(intermediate, index + 1, current, results)
        current.pop()

        current.append(1)
        generate_combinations_recursive(intermediate, index + 1, current, results)
        current.pop()


n_genes = 10
perturbation = 0.01
# setup predicor set manually

f_vars = np.array([[1], 
                   [0], 
                   [0], 
                   [0], 
                   [0],
                   [0],
                   [0],
                   [0],
                   [0],
                   [0]])

print('\n', "Predictor set of initial network:", '\n', f_vars)

# List to store generated arrays
arr_list = []

# Number of unique arrays to generate
num_arrays = 100

while len(arr_list) < num_arrays:
    while True:
    # Generate new array
    # generate random functions

        bns = [None]
        n_vars = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        bns = bn.generate_bn(n_genes, n_vars)
        funcs = bns.funcs

                # Check if new array is unique
        unique = True
        for a in arr_list:
            if np.array_equal(a, funcs):
                unique = False
                break

        # If new array is not unique, continue loop
        if not unique:
            continue

        arr_list.append(funcs)

        print('\n', "Functions of initial network:", '\n', funcs)

        additional = np.array ([[ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                                [ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1]])

        funcs_primary = funcs

        funcs = np.vstack((funcs, additional))

        ############################################################ genrate fully random network

        # max_vars = (round(math.sqrt(n_genes)))
        # bns = [None]

        # n_vars = random.choices(range(1, max_vars+1), k=n_genes)
        # bns = bn.generate_bn(n_genes, n_vars)

        # f_vars = bns.f_vars
        # funcs = bns.funcs

        ###################################################################

        net = GeneNetwork(n_genes, perturbation, f_vars, funcs)

        x = 0
        y = 3

        print('\n', "Indexes of selected genes to be merged:", '\n', x, y)

        SSD_primary = net.get_steady_state()

        # calculate expected SSD of the compressed networks

        b_indexes = np.zeros(shape=(0, 1))
        for i in range(len(SSD_primary)):
            b_indexes = np.append(b_indexes, utils.dec_to_bin(i, n_genes))
        SSD_primary_b = np.stack ((b_indexes, SSD_primary), axis=1)
        # np.savetxt('SSDprimary.csv', SSD_primary_b, fmt='%s', delimiter=',')

        undoubtful_00 = [j for i, j in SSD_primary_b if i[x]=='0' and i[y]=='0']
        undoubtful_11 = [j for i, j in SSD_primary_b if i[x]=='1' and i[y]=='1']
        doubtful_01 = [j for i, j in SSD_primary_b if i[x]=='0' and i[y]=='1']
        doubtful_10 = [j for i, j in SSD_primary_b if i[x]=='1' and i[y]=='0']

        undoubtful = undoubtful_00 + undoubtful_11
        doubtful_01eq0and10eq1 = doubtful_01 + doubtful_10
        doubtful_01eq1and10eq0 = doubtful_10 + doubtful_01

        undoubtful_f = [float(x) for x in undoubtful]
        doubtful_01eq0and10eq1_f = [float(x) for x in doubtful_01eq0and10eq1]
        doubtful_01eq1and10eq0_f = [float(x) for x in doubtful_01eq1and10eq0]

        expected_SSD_1 = [a + b for a, b in zip(undoubtful_f, doubtful_01eq0and10eq1_f)]
        expected_SSD_2 = [a + b for a, b in zip(undoubtful_f, doubtful_01eq1and10eq0_f)]

        # calculate average expected SSD

        expected_SSD = []
        for i in range(len(expected_SSD_1)):
            average = (expected_SSD_1[i] + expected_SSD_2[i]) / 2
            expected_SSD.append(average)
        # np.savetxt('expected_SSD.csv', expected_SSD, fmt='%s', delimiter=',')

        ####################################################################
        # ############### calculate the CP of primary

        CP_primary = net.get_canalizing_power([0,1,2,3,4,5,6,7,8,9], range(10), 2, net.get_steady_state())
        # print('\n', "CP primary:", '\n', CP_primary)
        # np.savetxt('CP_primary.csv', CP_primary, fmt='%.4e', delimiter=',')

        CP_primary = np.delete(CP_primary, [x, y], axis=0)

        # print('\n', "CP of primary network (without genes to be compressed):", '\n', CP_primary, '\n',)

        temp = CP_primary.argsort()
        ranks_CP_primary = np.empty_like(temp)
        ranks_CP_primary[temp] = np.arange(len(CP_primary))

        # print('\n', "CP rank of primary network (without genes to be compressed):", '\n', ranks_CP_primary, '\n',)

        ####################find attractors for primary network
        attractors_primary = net.get_attractors()

        b_indexes = np.zeros(shape=(0, 1))
        for i in range(2**n_genes):
            b_indexes = np.append(b_indexes, utils.dec_to_bin(i, n_genes))

        attractors_primary = np.column_stack ((b_indexes, attractors_primary))

        # print('\n', "Attractors and basins of primary network:", '\n',attractors_primary)

        basins_primary = np.unique(attractors_primary[:, 1])
        basins_primary = basins_primary.size

        print('\n', "Number of basins in primary network:", '\n', basins_primary)

        attractors_states_primary = attractors_primary[attractors_primary[: , 2] == '0.0'][: , 0]
        # np.savetxt('actual_attractors_primary.csv', attractors_states_primary, fmt='%s', delimiter=',')

        # print('\n', "Actual attractors of primary network:", '\n', attractors_states_primary)

        num_attractors_primary = len(attractors_states_primary)
        print('\n', "Number of attractor states (all basins) in primary:", '\n', num_attractors_primary)

        result_primary = list(map(lambda s: "".join([c for i, c in enumerate(str(s)) if i not in {x, y}]), attractors_states_primary))
        #print('\n', "Actual attractors of primary network (without genes to be compressed):", '\n', result_primary)


        ####################################################################

        # # Compress the network
        n_genes_c = n_genes - 1
        f_vars_r = f_vars.copy()

        for i in np.nditer(f_vars_r, op_flags = ['readwrite']):
            if i == -1:
                continue
            elif i < x: 
                i[...] += 1
            elif i == x:
                i[...] = 0
            elif i > x and i < y:
                continue
            elif i == y:
                i[...] = 0
            else:
                i[...] -=1

        ## remove duplicated predictors
        # convert your numpy array to a list
        f_vars_r = f_vars_r.tolist()
        # iterate over the list (sub will contain the sublists)
        for sub in f_vars_r:
            # get the indices of zeros from sub
            zero_indices = [i for i, n in enumerate(sub) if n == 0]
            if len(zero_indices) == 2:
                print('\n', "Execution terminated due to double connection to the merged gene", '\n')
                raise SystemExit
                ### the next two rows to be activated after we have a concept of what to do with the function
                # sub.pop(zero_indices[1])  # remove the second zero
                # sub.append(-1)  # add a -1 to the end


        # convert the list back to a numpy array
        f_vars_r = np.array(f_vars_r)
        # print('\n', "Re-indexed predictor set of primary network:", '\n', f_vars_r)

        f_vars_r_m = Functions.combineRows(f_vars_r, x, y)

        Functions.removeDuplicatesAndReorder(f_vars_r_m)
        print('\n', "Predictor set of compressed network:", '\n', f_vars_r_m)

        # # # # # delete and assign all possible functions
        # # # # # delete rows (functions) for the merged genes
        # # # # # funcs_c is funcs with deleted merged genes
        funcs_c = np.delete(funcs, [x, y], axis=1)


        #### execute the logical merge

        a = funcs[:, x]

        print('\n', "Function of gene x to be merged:", '\n', a)


        a_i = np.array([a[0], a[0], a[1], a[1]])

        b = funcs[:, y]

        print('\n', "Function of gene y to be merged:", '\n', b)

        b_i = np.array([b[0], b[1], b[0], b[1]])

        intermediate = np.mean([a_i, b_i], axis = 0)
        
        # Примерно извикване

        combinations = generate_combinations(intermediate)
        logical_merged_function = combinations.astype(int)
        print('\n', "Set of logical merged functions:", '\n', logical_merged_function)

        
        ##################################


        # Initialize the variable to a large or small value to ensure it gets updated in the loop
        min_nSAD = float('inf')
        max_nSAD = float('-inf')


        min_nHammingCP = float('inf')
        max_nHammingCP = float('-inf')

        min_percent_full_match = float('inf')
        max_percent_full_match = float('-inf')

        basin_closest_1 = None
        basin_farthest_1 = None

        attractors_closest_1 = None
        attractors_farthest_1 = None

        n = 0
        for i in logical_merged_function:
            
            # construnct function set of compressed
            funcs_c = np.column_stack((logical_merged_function[n], funcs_c))
            print('\n', "Functions of the compressed network:", '\n',funcs_c)
            function_meta = funcs_c [:, 0]
            print('\n', "Function of the metagene:", '\n',function_meta)
        
            # construnct compressed network
            net_compressed = GeneNetwork(n_genes_c, perturbation, f_vars_r_m, funcs_c)
            # find SSD of compressed
            SSD_compressed = net_compressed.get_steady_state()
            
            ########################## find nSAD
            Difference = np.subtract(expected_SSD, SSD_compressed)  
            AbsoluteDifference = abs(Difference) 
            SumOfAbsoluteDifference = np.sum(AbsoluteDifference)
            NormalizedSumOfAbsoluteDifference = SumOfAbsoluteDifference/(2**n_genes_c)
            # NormalizedSumOfAbsoluteDifference = f'{NormalizedSumOfAbsoluteDifference:.10f}'

            # with open('comprehensive testing.csv', 'a') as f_object:
            #     writer_object = writer(f_object)
            #     writer_object.writerow([NormalizedSumOfAbsoluteDifference])
            #     f_object.close()

            print('\n', "nSAD:", '\n', NormalizedSumOfAbsoluteDifference)

            if NormalizedSumOfAbsoluteDifference < min_nSAD:
                min_nSAD = NormalizedSumOfAbsoluteDifference
                min_nSAD_func = [function_meta]
            elif min_nSAD == NormalizedSumOfAbsoluteDifference:
                min_nSAD_func.append(function_meta)

            if NormalizedSumOfAbsoluteDifference > max_nSAD:
                max_nSAD = NormalizedSumOfAbsoluteDifference
                max_nSAD_func = [function_meta]
            elif max_nSAD == NormalizedSumOfAbsoluteDifference:
                max_nSAD_func.append(function_meta)
            ################################## end of nSAD part
            # ############### calculate the CP of compressed

            CP_compressed = net_compressed.get_canalizing_power([0,1,2,3,4,5,6,7,8], range(9), 2, net_compressed.get_steady_state())
            CP_compressed = np.delete(CP_compressed, [0], axis=0)

            temp = CP_compressed.argsort()
            ranks_CP_compressed = np.empty_like(temp)
            ranks_CP_compressed[temp] = np.arange(len(CP_compressed))

            # print('\n', "CP rank of compressed network (without the metagene):", '\n', ranks_CP_compressed, '\n',)

            Hamming_distance_CP_ranks = np.array([ranks_CP_primary, ranks_CP_compressed, Functions.hamming2(ranks_CP_primary, ranks_CP_compressed)], dtype=object)
            Hamming_distance_CP_ranks = [arr.tolist() if isinstance(arr, np.ndarray) else [str(arr)] for arr in Hamming_distance_CP_ranks]
            Hamming_distance_CP_ranks[-1] = [str(int(Hamming_distance_CP_ranks[-1][0]) / (n_genes-2))]

            OnlyHammingCP = Hamming_distance_CP_ranks[-1]
            print('\n', "Hamming distance between CP rank of primary vs. compressed network, normalized:", '\n', OnlyHammingCP)
            
            OnlyHammingCP = float(OnlyHammingCP[0])

            if OnlyHammingCP < min_nHammingCP:
                min_nHammingCP = OnlyHammingCP
                min_nHammingCP_func = [function_meta]
            elif min_nHammingCP == OnlyHammingCP:
                min_nHammingCP_func.append(function_meta)

            if OnlyHammingCP > max_nHammingCP:
                max_nHammingCP = OnlyHammingCP
                max_nHammingCP_func = [function_meta]
            elif max_nHammingCP == OnlyHammingCP:
                max_nHammingCP_func.append(function_meta)

            
            ####################find basins, attractors number and actual attractors for compressed network and compare with primary
            # (ignoring genes to be compressed from the primary and the metagene from the compressed)

            # find attractors (and basins) for compressed network
            attractors_compressed = net_compressed.get_attractors()
            # np.savetxt('actual_attractors_compressed.csv', attractors_compressed, fmt='%s', delimiter=',')
            b_indexes = np.zeros(shape=(0, 1))
            for i in range(2**n_genes_c):
                b_indexes = np.append(b_indexes, utils.dec_to_bin(i, n_genes_c))

            attractors_compressed = np.column_stack ((b_indexes, attractors_compressed))

            attractors_states_compressed = attractors_compressed[attractors_compressed[: , 2] == '0.0'][: , 0]
            # np.savetxt('actual_attractors_compressed.csv', attractors_states_compressed, fmt='%s', delimiter=',')
            # print('\n', "Actual attractors of compressed network:", '\n', attractors_states_compressed)

            ################################## number of basins and number of attractors in compressed
            basins_compressed = np.unique(attractors_compressed[:, 1])
            basins_compressed = basins_compressed.size
            # print("Basins primary", basins_primary)
            # print("Basins compressed", basins_compressed)

            # print('\n', "Basins in primary vs. basins in compressed:", '\n', basins_primary, "-", basins_compressed)
            basin_ratio = basins_primary/basins_compressed
            print('\n', "Basins ratio primary/compressed:", '\n', basin_ratio)

            if basin_closest_1 is None or abs(basin_ratio - 1) < abs(basin_closest_1 - 1):
                basin_closest_1 = basin_ratio
                basin_closest_1_func = [function_meta]
            elif abs(basin_ratio - 1) == abs(basin_closest_1 - 1):
                basin_closest_1_func.append(function_meta)

            if basin_farthest_1 is None or abs(basin_ratio - 1) > abs(basin_farthest_1 - 1):
                basin_farthest_1 = basin_ratio
                basin_farthest_1_func = [function_meta]
            elif abs(basin_ratio - 1) == abs(basin_farthest_1 - 1):
                basin_farthest_1_func.append(function_meta)
            

            num_attractors_compressed = len(attractors_states_compressed)
            # print('\n', "Attractor states in primary vs. compressed (all basins of both):", '\n', num_attractors_primary, "-", num_attractors_compressed)
            attractor_ratio = num_attractors_primary/num_attractors_compressed
            print('\n', "Attractor ratio primary/compressed:", '\n', attractor_ratio)

            if attractors_closest_1 is None or abs(attractor_ratio - 1) < abs(attractors_closest_1 - 1):
                attractors_closest_1 = attractor_ratio
                attractors_closest_1_func = [function_meta]
            elif abs(attractor_ratio - 1) == abs(attractors_closest_1 - 1):
                attractors_closest_1_func.append(function_meta)

            if attractors_farthest_1 is None or abs(attractor_ratio - 1) > abs(attractors_farthest_1 - 1):
                attractors_farthest_1 = attractor_ratio
                attractors_farthest_1_func = [function_meta]
            elif abs(attractor_ratio - 1) == abs(attractors_farthest_1 - 1):
                attractors_farthest_1_func.append(function_meta)


            ################################## compare exact attractors states from primary versus compressed

            result_compressed = list(map(lambda s: "".join([c for i, c in enumerate(str(s)) if i not in {0}]), attractors_states_compressed))
            # print('\n', "Actual attractors of compressed network (without the metagene):", '\n', result_compressed, '\n')

            # generate an array only with the best fits for each attractor in the primary
            data = []
            for i in result_primary:
                for j in result_compressed:
                    primary_att_vs_compr_hamming = np.array([i, j, Functions.hamming2(i, j)])
                    data.append(primary_att_vs_compr_hamming)

            data = np.array(data)        
            # print(data)

            data_only_lowest = Functions.get_lowest_score_rows(data)
            data_only_lowest[:, 2] = data_only_lowest[:, 2].astype(int) / (n_genes-2)
            # print('\n', "Comparison primary attractors vs. compressed, normalized Hamming distance score (best fit only):", '\n', data_only_lowest, '\n')
            
            # calculate how many percent of the attractors of the primary network have full match in the attractors of the compressed network
            # select the third column and check how many elements are '0'
            num_zeros = np.count_nonzero(data_only_lowest[:, 2] == '0.0')

            # calculate the percentage of rows with '0' in the third column
            percent_full_match = (num_zeros / data_only_lowest.shape[0]) * 100
            print('\n', "Percents inherited exact attractor states", '\n', percent_full_match, '\n', '\n')
        
            if percent_full_match < min_percent_full_match:
                min_percent_full_match = percent_full_match
                min_percent_full_match_func = [function_meta]
            elif min_percent_full_match == percent_full_match:
                min_percent_full_match_func.append(function_meta)
            
            # print(min_percent_full_match_func)
            # print(type(min_percent_full_match_func))

            
            if percent_full_match > max_percent_full_match:
                max_percent_full_match = percent_full_match
                max_percent_full_match_func = [function_meta]
            elif max_percent_full_match == percent_full_match:
                max_percent_full_match_func.append(function_meta)
            ##################################

            funcs_c = np.delete(funcs_c, [0], axis=1)
            n = n+1


        # minimum and maximum nSAD
        # print("Minimum nSAD:", min_nSAD)
        # print("Maximum nSAD:", max_nSAD)

        # print("Minimal Hamming distance between CP rank of primary vs. compressed network, normalized:", min_nHammingCP)
        # print("Maximal Hamming distance between CP rank of primary vs. compressed network, normalized:", max_nHammingCP)

        # print("Minimum percents inherited exact atractor states", min_percent_full_match)
        # print("Maximum percents inherited exact atractor states", max_percent_full_match)

        # print("Basin ratio best:", basin_closest_1)
        # print("Basin ratio worst:", basin_farthest_1)
        # print("Functions where the closest to 1 basin ratio is observed:", basin_closest_1_func)
        # print("Functions where the farthest to 1 basin ratio is observed:", basin_farthest_1_func)

        # print("Attractors ratio best:", attractors_closest_1)
        # print("Attractors ratio worst:", attractors_farthest_1)
        # print("Attractors ratio best function:", attractors_closest_1_func)
        # print("Attractors ratio worst function:", attractors_farthest_1_func)

        # Define the header row of minimum file
        header = ['funcs_primary', 'min_nSAD_func', 'min_nSAD', 'min_nHammingCP_func', 'min_nHammingCP', 'basin_closest_1_func', 'basin_closest_1', 'attractors_closest_1_func', 'attractors_closest_1', 'max_percent_full_match_func', 'max_percent_full_match']

        # Open the CSV file in 'append' mode
        with open('results_best.csv', 'a', newline='') as f_object:
            # Check if the file is empty (to avoid adding a new header row)
            is_empty = os.stat('results_best.csv').st_size == 0
            
            # Create a CSV writer object and write the header row if necessary
            writer_object = csv.writer(f_object, delimiter = ';')
            if is_empty:
                writer_object.writerow(header)
            
            # Write the data row
            writer_object.writerow([funcs_primary, min_nSAD_func, min_nSAD, min_nHammingCP_func, min_nHammingCP, basin_closest_1_func, basin_closest_1,  attractors_closest_1_func, attractors_closest_1, max_percent_full_match_func, max_percent_full_match])


        # Define the header row of maximum file
        header = ['funcs_primary', 'max_nSAD_func', 'max_nSAD', 'max_nHammingCP_func', 'max_nHammingCP', 'basin_farthest_1_func', 'basin_farthest_1',  'attractors_farthest_1_func', 'attractors_farthest_1', 'min_percent_full_match_func', 'min_percent_full_match']

        # Open the CSV file in 'append' mode
        with open('results_worst.csv', 'a', newline='') as f_object:
            # Check if the file is empty (to avoid adding a new header row)
            is_empty = os.stat('results_worst.csv').st_size == 0
            
            # Create a CSV writer object and write the header row if necessary
            writer_object = csv.writer(f_object, delimiter = ';')
            if is_empty:
                writer_object.writerow(header)
            
            # Write the data row
            writer_object.writerow([funcs_primary, max_nSAD_func, max_nSAD, max_nHammingCP_func, max_nHammingCP, basin_farthest_1_func, basin_farthest_1, attractors_farthest_1_func, attractors_farthest_1, min_percent_full_match_func, min_percent_full_match])
        ########################################

        break


end_time = time.time()

total_time = end_time - start_time
minutes = total_time / 60

print("Time taken:", minutes, "minutes")