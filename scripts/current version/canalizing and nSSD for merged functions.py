from boolean_networks.steady_state import get_steady_state
from boolean_networks import utils, canalizing
from boolean_networks.gene_network import GeneNetwork
import matplotlib.pyplot as plt
from boolean_networks.MFPT import swap_MFPT, calculate_gamma
from boolean_networks import bn

import numpy as np
from numpy import genfromtxt
from numpy import savetxt
import numpy.ma as ma
import math, random, pickle, pandas, json
import networkx as nx
import pprint

import scipy as sp

from csv import writer

import Functions


n_genes = 10
perturbation = 0.01

f_vars = np.array([[1], 
                   [0], 
                   [0], 
                   [0], 
                   [0],
                   [0],
                   [0],
                   [0],
                   [0],
                   [0]])

# f_vars = np.array([[9], 
#                    [0], 
#                    [1], 
#                    [2], 
#                    [3],
#                    [4],
#                    [5],
#                    [6],
#                    [7],
#                    [8]])

# f_vars = np.array([[1], 
#                    [0], 
#                    [1], 
#                    [2], 
#                    [3],
#                    [4],
#                    [5],
#                    [6],
#                    [7],
#                    [8]])


funcs = np.array ([[ 0,  1,  0,  1,  0,  1,  0,  1,  0,  1],
                   [ 1,  0,  1,  0,  1,  0,  1,  0,  1,  0],
                   [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
                   [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]])

# funcs = np.array ([[ 0,  1,  0,  1,  0,  1,  0,  1,  0,  1],
#                    [ 1,  0,  1,  0,  1,  0,  1,  0,  1,  0]])  

# print('\n', "Predictor set of initial network:", '\n', f_vars)
# print('\n', "Functions of initial network:", '\n', funcs)

net = GeneNetwork(n_genes, perturbation, f_vars, funcs)
SSD_primary = net.get_steady_state()
# print('\n', "SSD of the primary network", '\n', SSD_primary)
# np.savetxt('SSD_primary.csv', SSD_primary, fmt='%.4e', delimiter=',')

# select genes to be compressed
x = 0
y = 3

# calculate expected SSD of the compressed networks
b_indexes = np.zeros(shape=(0, 1))
for i in range(len(SSD_primary)):
    b_indexes = np.append(b_indexes, utils.dec_to_bin(i, n_genes))
SSD_primary_b = np.stack ((b_indexes, SSD_primary), axis=1)

undoubtful_00 = [j for i, j in SSD_primary_b if i[x]=='0' and i[y]=='0']
undoubtful_11 = [j for i, j in SSD_primary_b if i[x]=='1' and i[y]=='1']
doubtful_01 = [j for i, j in SSD_primary_b if i[x]=='0' and i[y]=='1']
doubtful_10 = [j for i, j in SSD_primary_b if i[x]=='1' and i[y]=='0']

undoubtful = undoubtful_00 + undoubtful_11
doubtful_01eq0and10eq1 = doubtful_01 + doubtful_10
doubtful_01eq1and10eq0 = doubtful_10 + doubtful_01

undoubtful_f = [float(x) for x in undoubtful]
doubtful_01eq0and10eq1_f = [float(x) for x in doubtful_01eq0and10eq1]
doubtful_01eq1and10eq0_f = [float(x) for x in doubtful_01eq1and10eq0]

expected_SSD_1 = [a + b for a, b in zip(undoubtful_f, doubtful_01eq0and10eq1_f)]
expected_SSD_2 = [a + b for a, b in zip(undoubtful_f, doubtful_01eq1and10eq0_f)]

# calculate average expected SSD

expected_SSD = []
for i in range(len(expected_SSD_1)):
    average = (expected_SSD_1[i] + expected_SSD_2[i]) / 2
    expected_SSD.append(average)

# np.savetxt('expected_SSD.csv', expected_SSD, fmt='%s', delimiter=',')

# ############### calculate the CP of primary

CP_primary = net.get_canalizing_power([0,1,2,3,4,5,6,7,8,9], range(10), 2, net.get_steady_state())
# print('\n', "CP primary:", '\n', CP_primary)
# np.savetxt('CP_primary.csv', CP_primary, fmt='%.4e', delimiter=',')

CP_primary = np.delete(CP_primary, [x, y], axis=0)

# print('\n', "CP of primary network (without genes to be compressed):", '\n', CP_primary, '\n',)

temp = CP_primary.argsort()
ranks_CP_primary = np.empty_like(temp)
ranks_CP_primary[temp] = np.arange(len(CP_primary))

# print('\n', "CP rank of primary network (without genes to be compressed):", '\n', ranks_CP_primary, '\n',)

#find attractors for primary network
attractors = net.get_attractors()

b_indexes = np.zeros(shape=(0, 1))
for i in range(2**n_genes):
    b_indexes = np.append(b_indexes, utils.dec_to_bin(i, n_genes))

attractors = np.column_stack ((b_indexes, attractors))

# print('\n', "attractors of primary network:", '\n',attractors)

attractors_states = attractors[attractors[: , 2] == '0.0'][: , 0]
# np.savetxt('actual_attractors_primary.csv', attractors_states, fmt='%s', delimiter=',')

print('\n', "Actual attractors of primary network:", '\n', attractors_states)

result_primary = list(map(lambda s: "".join([c for i, c in enumerate(str(s)) if i not in {x, y}]), attractors_states))
# print('\n', "Actual attractors of primary network (without genes to be compressed):", '\n', result_primary)


# # # # Compress the network
f_vars_r = f_vars.copy()

for i in np.nditer(f_vars_r, op_flags = ['readwrite']):
    if i == -1:
        continue
    elif i < x: 
        i[...] += 1
    elif i == x:
        i[...] = 0
    elif i > x and i < y:
        continue
    elif i == y:
        i[...] = 0
    else:
        i[...] -=1

# # remove duplicated predictors
# # convert your numpy array to a list
f_vars_r = f_vars_r.tolist()
# iterate over the list (sub will contain the sublists)
for sub in f_vars_r:
    # get the indices of zeros from sub
    zero_indices = [i for i, n in enumerate(sub) if n == 0]
    if len(zero_indices) == 2:
        print('\n', "Execution terminated due to double connection to the merged gene", '\n')
        raise SystemExit
        ### the next two rows to be activated after we have a concept of what to do with the function
        # sub.pop(zero_indices[1])  # remove the second zero
        # sub.append(-1)  # add a -1 to the end


# # convert the list back to a numpy array
f_vars_r = np.array(f_vars_r)

# # # print('\n', "Re-indexed predictor set of primary network:", '\n', f_vars_r)

f_vars_r_m = Functions.combineRows(f_vars_r, x, y)

Functions.removeDuplicatesAndReorder(f_vars_r_m)
# print('\n', "Predictor set of compressed network:", '\n', f_vars_r_m)


### state transition diagram for the primary network

json_object = Functions.export_to_json(net)
with open("primary.json", "w") as outfile:
    outfile.write(json_object)  


# # ######## delete and assign all possible functions or all logically merged funcions - to be implemented

# # # delete rows (functions) for the merged genes
# # # funcs_c is funcs with deleted merged genes
funcs_c = np.delete(funcs, [x, y], axis=1)

# ### merged function manually set

merged_function = np.array([[ 0,  0,  1,  0],
                            [ 1,  0,  0,  0],
                            [ 1,  0,  1,  1],
                            [ 1,  1,  1,  0]])

# merged_function = np.array([[ 0,  1],
#                             [ 1,  0]])

n_genes_c = n_genes - 1

# #### the cycle should start here

counter = 0
n = 0
for i in merged_function:

    # assign merged function to the metagene
    funcs_c = np.column_stack((merged_function[n], funcs_c))
    print('\n', "Functions of compressed network):", '\n',funcs_c)
    # generate compressed network
    net_compressed = GeneNetwork(n_genes_c, perturbation, f_vars_r_m, funcs_c)
    
    ################# compare CP ranks

    # calculate canalization power of compressed network (d=2)
    CP_compressed = net_compressed.get_canalizing_power([0,1,2,3,4,5,6,7,8], range(9), 2, net_compressed.get_steady_state())
    # remove the metagene
    CP_compressed = np.delete(CP_compressed, [0], axis=0)

    # print('\n', "CP of compressed network (without the metagene):", '\n', CP_compressed, '\n',)

    # assign a rank to each gene by CP
    temp = CP_compressed.argsort()
    ranks_CP_compressed = np.empty_like(temp)
    ranks_CP_compressed[temp] = np.arange(len(CP_compressed))

    # print('\n', "CP rank of compressed network (without the metagene):", '\n', ranks_CP_compressed, '\n',)

    # find hamming distance between CP rabk of primary and compressed, normalized
    Hamming_distance_CP_ranks = np.array([ranks_CP_primary, ranks_CP_compressed, Functions.hamming2(ranks_CP_primary, ranks_CP_compressed)], dtype=object)
    Hamming_distance_CP_ranks = [arr.tolist() if isinstance(arr, np.ndarray) else [str(arr)] for arr in Hamming_distance_CP_ranks]
    Hamming_distance_CP_ranks[-1] = [str(int(Hamming_distance_CP_ranks[-1][0]) / (n_genes-2))]

    print('\n', "Hamming distance between CP rank of primary vs. compressed network, normalized:", '\n', Hamming_distance_CP_ranks, '\n',)
    
    ############ compare attractors from the primary with attractors from compressed 
    # (ignoring genes to be compressed from the primary and the metagene from the compressed)

    # find attractors for compressed network
    attractors = net_compressed.get_attractors()
    # np.savetxt('actual_attractors_compressed.csv', attractors, fmt='%s', delimiter=',')
    b_indexes = np.zeros(shape=(0, 1))
    for i in range(2**n_genes_c):
        b_indexes = np.append(b_indexes, utils.dec_to_bin(i, n_genes_c))

    attractors = np.column_stack ((b_indexes, attractors))

    attractors_states = attractors[attractors[: , 2] == '0.0'][: , 0]
    # np.savetxt('actual_attractors_compressed.csv', attractors_states, fmt='%s', delimiter=',')
    print('\n', "Actual attractors of compressed network:", '\n', attractors_states)

    result_compressed = list(map(lambda s: "".join([c for i, c in enumerate(str(s)) if i not in {0}]), attractors_states))
    print('\n', "Actual attractors of compressed network (without the metagene):", '\n', result_compressed, '\n')

    # generate an array only with the best fits for each attractor in the primary
    data = []
    for i in result_primary:
        for j in result_compressed:
            primary_att_vs_compr_hamming = np.array([i, j, Functions.hamming2(i, j)])
            data.append(primary_att_vs_compr_hamming)

    data = np.array(data)        
    
    # print(data)

    data_only_lowest = Functions.get_lowest_score_rows(data)
    data_only_lowest[:, 2] = data_only_lowest[:, 2].astype(int) / (n_genes-2)
    print('\n', "Comparison primary attractors vs. compressed, normalized Hamming distance score (best fit only):", '\n', data_only_lowest, '\n')

    # state transition diagram for the compressed network

    filename = f"compressed_{counter}.json"

    # it can be done this way with time stamp (do not forget to "import time")
    # filename = f"compressed_{time.time()}-{counter}.json"

    counter += 1

    json_object = Functions.export_to_json(net_compressed)
    with open(filename, "w") as outfile:
        outfile.write(json_object)  

    funcs_c = np.delete(funcs_c, [0], axis=1)
    n = n+1