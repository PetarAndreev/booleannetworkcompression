from boolean_networks import utils
from boolean_networks.gene_network import GeneNetwork
from boolean_networks import bn

import numpy as np
import time

import csv
import os

import Functions




# # ###################################### this is 16 bit functions (4 predictors)
# # ##### in brackets assign the number of functions that will have to be assigned - for 4 predictors 65536 as
# # ##### 4 predictors have 16 possible combinations, and we need to have one function per each)

comprehensive_merged_function = np.zeros(shape=(0, 1))
for i in range(65536):
    comprehensive_merged_function = np.append(comprehensive_merged_function, utils.dec_to_bin(i, 16))

comprehensive_merged_function = np.array([list(s) for s in comprehensive_merged_function]).astype(int)

print(comprehensive_merged_function)
np.savetxt('comprehensive merged function.csv', comprehensive_merged_function, fmt='%.4e', delimiter=',')

# Проверка на редовете за 8 битова функция
valid_rows = []
for row in comprehensive_merged_function:
    # Критерий (1)
    if row[0] == row[8] and row[1] == row[9] and row[2] == row[10] and row[3] == row[11] and row[4] == row[12] and row[5] == row[13] and row[6] == row[14] and row[7] == row[15]:
        continue  # Пропускане на реда и продължаване със следващия
    # Критерий (2)
    if row[0] == row[4] and row[1] == row[5] and row[2] == row[6] and row[3] == row[7] and row[8] == row[12] and row[9] == row[13] and row[10] == row[14] and row[11] == row[15]:
        continue
    # Критерий (3)
    if row[0] == row[2] and row[1] == row[3] and row[4] == row[6] and row[5] == row[7] and row[8] == row[10] and row[9] == row[11] and row[12] == row[14] and row[13] == row[15]:
        continue
     # Критерий (4)
    if row[0] == row[1] and row[2] == row[3] and row[4] == row[5] and row[6] == row[7] and row[8] == row[9] and row[10] == row[11] and row[12] == row[13] and row[14] == row[15]:
        continue
    valid_rows.append(row)

# Новият масив с валидните редове
valid_array = np.array(valid_rows)
print(valid_array)
np.savetxt('only valid.csv', valid_array, fmt='%.4e', delimiter=',')

# # ################################################

# ######################################## this is for 8 bit functions (3 predictors)
# appending_indexes = np.zeros((256, 8), dtype=int) - 1                      
# comprehensive_merged_function = np.zeros(shape=(0, 1), dtype=int)

# for i in range(256):
#     comprehensive_merged_function = np.append(comprehensive_merged_function, utils.dec_to_bin(i, 8))

# comprehensive_merged_function = np.array([list(s) for s in comprehensive_merged_function]).astype(int)
# comprehensive_merged_function = np.hstack([comprehensive_merged_function, appending_indexes])

# print('\n', "Comprehensive merged function:", '\n', comprehensive_merged_function)
# np.savetxt('comprehensive merged function.csv', comprehensive_merged_function, fmt='%.4e', delimiter=',')

# print(type(comprehensive_merged_function))

# # Проверка на редовете за 8 битова функция
# valid_rows = []
# for row in comprehensive_merged_function:
#     # Критерий (1)
#     if row[0] == row[4] and row[1] == row[5] and row[2] == row[6] and row[3] == row[7]:
#         continue  # Пропускане на реда и продължаване със следващия
#     # Критерий (2)
#     if row[0] == row[2] and row[1] == row[3] and row[4] == row[6] and row[5] == row[7]:
#         continue
#     # Критерий (3)
#     if row[0] == row[1] and row[2] == row[3] and row[4] == row[5] and row[6] == row[7]:
#         continue
#     valid_rows.append(row)

# # Новият масив с валидните редове
# valid_array = np.array(valid_rows)
# print(valid_array)
# np.savetxt('only valid.csv', valid_array, fmt='%.4e', delimiter=',')

# ##############################################################