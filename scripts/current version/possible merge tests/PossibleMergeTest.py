from boolean_networks.steady_state import get_steady_state
from boolean_networks import utils, canalizing
from boolean_networks.gene_network import GeneNetwork
import matplotlib.pyplot as plt
from boolean_networks.MFPT import swap_MFPT, calculate_gamma
from boolean_networks import bn

import numpy as np
from numpy import genfromtxt
from numpy import savetxt
import numpy.ma as ma
import math, random, pickle, pandas, json
import networkx as nx
import pprint

import scipy as sp

from csv import writer

import Functions


n_genes = 10
perturbation = 0.01


f_vars = np.array([[3, 4, -1,-1],		
                   [7, 8, 9, -1],		
                   [2, 3, 9, -1],		
                   [0, 1, 2, -1],		
                   [0, 2, -1, -1],		
                   [0, 1, 2, -1],		
                   [0, 1, 3, 4],		
                   [3, 4, 5, -1],		
                   [6, 7, -1, -1],		
                   [6, 7, -1, -1]])		



funcs = np.array([[0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                  [1, 0, 0, 1, 1, 1, 0, 0, 0, 1],
                  [1, 0, 0, 1, 0, 1, 0, 1, 0, 1],
                  [0, 1, 0, 1, 0, 0, 1, 0, 1, 0],
                  [-1, 0, 0, 1, -1, 1, 1, 1, -1, -1],
                  [-1, 0, 0, 1, -1, 1, 1, 1, -1, -1],
                  [-1, 0, 0, 1, -1, 1, 1, 1, -1, -1],
                  [-1, 0, 1, 1, -1, 1, 1, 1, -1, -1],
                  [-1, -1, -1, -1, -1, -1, 0, -1, -1, -1],
                  [-1, -1, -1, -1, -1, -1, 0, -1, -1, -1],
                  [-1, -1, -1, -1, -1, -1, 0, -1, -1, -1],
                  [-1, -1, -1, -1, -1, -1, 1, -1, -1, -1],
                  [-1, -1, -1, -1, -1, -1, 0, -1, -1, -1],
                  [-1, -1, -1, -1, -1, -1, 0, -1, -1, -1],
                  [-1, -1, -1, -1, -1, -1, 0, -1, -1, -1],
                  [-1, -1, -1, -1, -1, -1, 1, -1, -1, -1]])


print('\n', "Predictor set of initial network:", '\n', f_vars)
# print('\n', "Functions of initial network:", '\n', funcs)

net = GeneNetwork(n_genes, perturbation, f_vars, funcs)

# select genes to be compressed
x = 8
y = 9

# Compress the network
f_vars_r = f_vars.copy()

for i in np.nditer(f_vars_r, op_flags = ['readwrite']):
    if i == -1:
        continue
    elif i < x: 
        i[...] += 1
    elif i == x:
        i[...] = 0
    elif i > x and i < y:
        continue
    elif i == y:
        i[...] = 0
    else:
        i[...] -=1

# # remove duplicated predictors
# # convert your numpy array to a list
f_vars_r = f_vars_r.tolist()
# iterate over the list (sub will contain the sublists)
for sub in f_vars_r:
    # get the indices of zeros from sub
    zero_indices = [i for i, n in enumerate(sub) if n == 0]
    # if len(zero_indices) == 2:
    #     print('\n', "Execution terminated due to double connection to the merged gene", '\n')
    #     raise SystemExit
        ### the next two rows to be activated after we have a concept of what to do with the function
        # sub.pop(zero_indices[1])  # remove the second zero
        # sub.append(-1)  # add a -1 to the end


# # convert the list back to a numpy array
f_vars_r = np.array(f_vars_r)

print('\n', "Re-indexed predictor set of primary network:", '\n', f_vars_r)

f_vars_r_m = Functions.combineRows(f_vars_r, x, y)

Functions.removeDuplicatesAndReorder(f_vars_r_m)
print('\n', "Predictor set of compressed network:", '\n', f_vars_r_m)