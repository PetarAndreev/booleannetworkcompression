from boolean_networks import utils
from boolean_networks.gene_network import GeneNetwork
from boolean_networks import bn

import numpy as np
import time

import csv
import os

import Functions



start_time = time.time()

n_genes = 9
perturbation = 0.01

# setup predicor set manually

f_vars = np.array([[ 1,  8],
 [ 0, -1],
 [ 1, -1],
 [ 2, -1],
 [ 3, -1],
 [ 4, -1],
 [ 5, -1],
 [ 6, -1],
 [ 7, -1]])

funcs = np.array( [[ 1,  1,  0,  1,  1,  1,  1,  1,  1],
                   [ 0,  0,  1,  0,  0,  0,  0,  0,  0],
                   [ 0, -1, -1, -1, -1, -1, -1, -1, -1],
                   [ 0, -1, -1, -1, -1, -1, -1, -1, -1]])

print('\n', "Functions of initial network:", '\n', funcs)

net = GeneNetwork(n_genes, perturbation, f_vars, funcs)



json_object = Functions.export_to_json(net)
with open('statediagram.json', "w") as outfile:
        outfile.write(json_object)