import networkx as nx
import numpy as np
import matplotlib.pyplot as plt

# Създаване на граф от матрицата f_vars
f_vars = np.array([[3, 4, -1, -1],
                   [7, 8, 9, -1],
                   [2, 3, 9, -1],
                   [0, 1, 2, -1],
                   [0, 2, -1, -1],
                   [0, 1, 2, -1],
                   [0, 1, 3, 4],
                   [3, 4, 5, -1],
                   [6, 7, -1, -1],
                   [6, 7, -1, -1]])

num_nodes = f_vars.shape[0]

G = nx.DiGraph()

# Добавяне на върховете към графа
G.add_nodes_from(range(num_nodes))

# Добавяне на ребрата към графа
for i in range(num_nodes):
    dependencies = f_vars[i, f_vars[i] != -1]
    G.add_edges_from([(j, i) for j in dependencies])


# Идентификация на циклични структури
cycles = list(nx.simple_cycles(G))

# Отпечатване на резултата
for cycle in cycles:
    print(cycle)