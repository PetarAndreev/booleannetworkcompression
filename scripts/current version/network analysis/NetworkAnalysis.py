import networkx as nx
import numpy as np
import matplotlib.pyplot as plt

# Създаване на граф от матрицата f_vars
f_vars = np.array([[3, 4, -1, -1],
                   [7, 8, 9, -1],
                   [2, 3, 9, -1],
                   [0, 1, 2, -1],
                   [0, 2, -1, -1],
                   [0, 1, 2, -1],
                   [0, 1, 3, 4],
                   [3, 4, 5, -1],
                   [6, 7, -1, -1],
                   [6, 7, -1, -1]])

num_nodes = f_vars.shape[0]

G = nx.DiGraph()

# Добавяне на върховете към графа
G.add_nodes_from(range(num_nodes))

# Добавяне на ребрата към графа
for i in range(num_nodes):
    dependencies = f_vars[i, f_vars[i] != -1]
    G.add_edges_from([(j, i) for j in dependencies])

graph_variable = G

# Изобразяване на първоначалния граф
pos = nx.circular_layout(G)  # Определете подходящо разположение на върховете
nx.draw(G, pos, with_labels=True, node_color='lightblue', node_size=500, arrowstyle='->', arrowsize=10, edge_color='gray')
plt.title("Първоначален граф")
plt.savefig("circular_layout.jpg", dpi = 300)

plt.clf()

def find_linear_regions(graph, start):
    visited = set()
    linear_regions = []

    def dfs(node, region):
        visited.add(node)
        region.append(node)

        neighbors = graph[node]

        if len(neighbors) == 2:
            for neighbor in neighbors:
                if neighbor not in visited:
                    dfs(neighbor, region)

    for node in graph.nodes():
        if node not in visited:
            region = []
            dfs(node, region)

            if len(region) > 2:
                linear_regions.append(region)

    return linear_regions


# G = nx.from_numpy_array(f_vars)

linear_regions = find_linear_regions(G, start=0)

if len(linear_regions) > 0:
    print("Графът съдържа следните линейни области:")
    for region in linear_regions:
        print(region)
else:
    print("Графът не съдържа линейни области.")

# Визуализиране на линейните области
pos = nx.circular_layout(G)
nx.draw_networkx_nodes(G, pos, node_color='lightblue', node_size=500)
nx.draw_networkx_edges(G, pos, edge_color='gray')
nx.draw_networkx_labels(G, pos, font_color='black', font_size=10)

for region in linear_regions:
    region_edges = [(region[i], region[i+1]) for i in range(len(region) - 1)]
    nx.draw_networkx_edges(G, pos, edgelist=region_edges, edge_color='red')

plt.axis('off')


plt.savefig("linear areas.jpg", dpi = 300)