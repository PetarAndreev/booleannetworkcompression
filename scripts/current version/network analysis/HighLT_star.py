import networkx as nx
import numpy as np
import matplotlib.pyplot as plt

# Създаване на граф от матрицата f_vars
f_vars = np.array([[3, 4, -1, -1],
                   [7, 8, 9, -1],
                   [2, 3, 9, -1],
                   [0, 1, 2, -1],
                   [0, 2, -1, -1],
                   [0, 1, 2, -1],
                   [0, 1, 3, 4],
                   [3, 4, 5, -1],
                   [6, 7, -1, -1],
                   [6, 7, -1, -1]])

num_nodes = f_vars.shape[0]

G = nx.DiGraph()

# Добавяне на върховете към графа
G.add_nodes_from(range(num_nodes))

# Добавяне на ребрата към графа
for i in range(num_nodes):
    dependencies = f_vars[i, f_vars[i] != -1]
    G.add_edges_from([(j, i) for j in dependencies])

graph_variable = G

# Изобразяване на първоначалния граф
pos = nx.circular_layout(G)  # Определете подходящо разположение на върховете
nx.draw(G, pos, with_labels=True, node_color='lightblue', node_size=500, arrowstyle='->', arrowsize=10, edge_color='gray')
plt.title("Първоначален граф")
plt.savefig("circular_layout.jpg", dpi = 300)

# Върхове и ребра за подчертаване
highlight_nodes = [1, 3, 5, 6, 7, 8, 9]
highlight_edges = [(1, 3), (1, 5), (1, 6), (7, 1), (8, 1), (9, 1)]

# Визуализация на графа
pos = nx.circular_layout(G)
nx.draw(G, pos, with_labels=True, node_color='lightblue', node_size=500, arrowstyle='->', arrowsize=10, edge_color='gray')

# Подчертаване на върховете
nx.draw_networkx_nodes(G, pos, nodelist=highlight_nodes, node_color='red', node_size=500)

# Подчертаване на ребрата
nx.draw_networkx_edges(G, pos, edgelist=highlight_edges, width=2.0, alpha=1.0, edge_color='red')

plt.title("Подчертани върхове и ребра")
plt.savefig("highlighted_graph.jpg", dpi=300)
plt.show()