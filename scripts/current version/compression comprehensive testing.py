from boolean_networks.steady_state import get_steady_state
from boolean_networks import utils, canalizing
from boolean_networks.gene_network import GeneNetwork
import matplotlib.pyplot as plt
from boolean_networks.MFPT import swap_MFPT, calculate_gamma
from boolean_networks import bn

import numpy as np
from numpy import genfromtxt
from numpy import savetxt
import numpy.ma as ma
import math, random, pickle, pandas, json
import networkx as nx
import pprint

from csv import writer

import Functions


# URS-1 Setup primary network, provide as input the number of desired genes

n_genes = 10

perturbation = 0.01

# f_vars = np.array([[1], 
#                    [0], 
#                    [1], 
#                    [2]])


# funcs = np.array ([[ 0,  1,  0,  1],
#                    [ 1,  0,  1,  0],
#                    [-1, -1, -1, -1],
#                    [-1, -1, -1, -1]])

f_vars = np.array([[1], 
                   [0], 
                   [0], 
                   [0], 
                   [0],
                   [0],
                   [0],
                   [0],
                   [0],
                   [0]])

# f_vars = np.array([[1], 
#                    [0], 
#                    [1], 
#                    [2], 
#                    [3],
#                    [4],
#                    [5],
#                    [6],
#                    [7],
#                    [8]])


# funcs = np.array ([[ 0,  1,  0,  1,  0,  1,  0,  1,  0,  1],
#                    [ 1,  0,  1,  0,  1,  0,  1,  0,  1,  0],
#                    [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1],
#                    [-1, -1, -1, -1, -1, -1, -1, -1, -1, -1]])

funcs = np.array ([[ 0,  1,  0,  1,  0,  1,  0,  1,  0,  1],
                   [ 1,  0,  1,  0,  1,  0,  1,  0,  1,  0]])                   

print('\n', "Predictor set of initial network:", '\n', f_vars)
print('\n', "Functions of initial network:", '\n', funcs)

net = GeneNetwork(n_genes, perturbation, f_vars, funcs)

x = 2
y = 3

print('\n', "Indexes of selected genes to be merged:", '\n', x, y)

SSD_primary = net.get_steady_state()

# calculate expected SSD of the compressed networks
b_indexes = np.zeros(shape=(0, 1))
for i in range(len(SSD_primary)):
    b_indexes = np.append(b_indexes, utils.dec_to_bin(i, n_genes))
SSD_primary_b = np.stack ((b_indexes, SSD_primary), axis=1)
# np.savetxt('SSDprimary.csv', SSD_primary_b, fmt='%s', delimiter=',')

undoubtful_00 = [j for i, j in SSD_primary_b if i[x]=='0' and i[y]=='0']
undoubtful_11 = [j for i, j in SSD_primary_b if i[x]=='1' and i[y]=='1']
doubtful_01 = [j for i, j in SSD_primary_b if i[x]=='0' and i[y]=='1']
doubtful_10 = [j for i, j in SSD_primary_b if i[x]=='1' and i[y]=='0']

undoubtful = undoubtful_00 + undoubtful_11
doubtful_01eq0and10eq1 = doubtful_01 + doubtful_10
doubtful_01eq1and10eq0 = doubtful_10 + doubtful_01

undoubtful_f = [float(x) for x in undoubtful]
doubtful_01eq0and10eq1_f = [float(x) for x in doubtful_01eq0and10eq1]
doubtful_01eq1and10eq0_f = [float(x) for x in doubtful_01eq1and10eq0]

expected_SSD_1 = [a + b for a, b in zip(undoubtful_f, doubtful_01eq0and10eq1_f)]
expected_SSD_2 = [a + b for a, b in zip(undoubtful_f, doubtful_01eq1and10eq0_f)]

# np.savetxt('expected_SSD1.csv', expected_SSD_1, fmt='%s', delimiter=',')
# np.savetxt('expected_SSD2.csv', expected_SSD_2, fmt='%s', delimiter=',')

# calculate average expected SSD

expected_SSD = []
for i in range(len(expected_SSD_1)):
    average = (expected_SSD_1[i] + expected_SSD_2[i]) / 2
    expected_SSD.append(average)

# np.savetxt('expected_SSD.csv', expected_SSD, fmt='%s', delimiter=',')


# # Compress the network
n_genes_c = n_genes - 1
f_vars_r = f_vars.copy()

for i in np.nditer(f_vars_r, op_flags = ['readwrite']):
    if i == -1:
        continue
    elif i < x: 
        i[...] += 1
    elif i == x:
        i[...] = 0
    elif i > x and i < y:
        continue
    elif i == y:
        i[...] = 0
    else:
        i[...] -=1

# remove duplicated predictors
# convert your numpy array to a list
f_vars_r = f_vars_r.tolist()
# iterate over the list (sub will contain the sublists)
for sub in f_vars_r:
    # get the indices of zeros from sub
    zero_indices = [i for i, n in enumerate(sub) if n == 0]
    if len(zero_indices) == 2:
        print('\n', "Execution terminated due to double connection to the merged gene", '\n')
        raise SystemExit
        ### the next two rows to be activated after we have a concept of what to do with the function
        # sub.pop(zero_indices[1])  # remove the second zero
        # sub.append(-1)  # add a -1 to the end


# convert the list back to a numpy array
f_vars_r = np.array(f_vars_r)

# print('\n', "Re-indexed predictor set of primary network:", '\n', f_vars_r)

f_vars_r_m = Functions.combineRows(f_vars_r, x, y)

Functions.removeDuplicatesAndReorder(f_vars_r_m)
print('\n', "Predictor set of compressed network:", '\n', f_vars_r_m)

# # # # delete and assign all possible functions
# # # # delete rows (functions) for the merged genes
# # # # funcs_c is funcs with deleted merged genes
funcs_c = np.delete(funcs, [x, y], axis=1)



# # ###################################### this is 16 bit functions (4 predictors)
# # ##### in brackets assign the number of functions that will have to be assigned - for 4 predictors 65536 as
# # ##### 4 predictors have 16 possible combinations, and we need to have one function per each)

# comprehensive_merged_function = np.zeros(shape=(0, 1))
# for i in range(65536):
#     comprehensive_merged_function = np.append(comprehensive_merged_function, utils.dec_to_bin(i, 16))

# comprehensive_merged_function = np.array([list(s) for s in comprehensive_merged_function]).astype(int)
# print(comprehensive_merged_function)
# np.savetxt('comprehensive merged function.csv', comprehensive_merged_function, fmt='%.4e', delimiter=',')
# # ################################################

# # ######################################## this is for 8 bit functions (3 predictors)
# appending_indexes = np.zeros((256, 8), dtype=int) - 1                      
# comprehensive_merged_function = np.zeros(shape=(0, 1), dtype=int)

# for i in range(256):
#     comprehensive_merged_function = np.append(comprehensive_merged_function, utils.dec_to_bin(i, 8))

# comprehensive_merged_function = np.array([list(s) for s in comprehensive_merged_function]).astype(int)
# comprehensive_merged_function = np.hstack([comprehensive_merged_function, appending_indexes])

# # np.savetxt('comprehensive merged function.csv', comprehensive_merged_function, fmt='%.4e', delimiter=',')
# print(comprehensive_merged_function)
# print('\n', "Comprehensive merged function:", '\n', comprehensive_merged_function)
# # ##############################################################


######################################## this is for 4 bit functions (2 predictors)

# appending_indexes = np.zeros((16, 4), dtype=int) - 1          
# comprehensive_merged_function = np.zeros(shape=(0, 1), dtype=int)

# for i in range(16):
#     comprehensive_merged_function = np.append(comprehensive_merged_function, utils.dec_to_bin(i, 4))

# comprehensive_merged_function = np.array([list(s) for s in comprehensive_merged_function]).astype(int)
# print('\n', "Comprehensive merged function:", '\n', comprehensive_merged_function)

##############################################################

####################################### this is for 2 bit functions (1 predictors)

appending_indexes = np.zeros((4, 2), dtype=int) - 1          
comprehensive_merged_function = np.zeros(shape=(0, 1), dtype=int)

for i in range(4):
    comprehensive_merged_function = np.append(comprehensive_merged_function, utils.dec_to_bin(i, 2))

# print(comprehensive_merged_function)
comprehensive_merged_function = np.array([list(s) for s in comprehensive_merged_function]).astype(int)

######################################################################################################
# counter = 0
n = 0
for i in comprehensive_merged_function:
    
    funcs_c = np.column_stack((comprehensive_merged_function[n], funcs_c))
    print('\n', "Functions of compressed network:", '\n',funcs_c)
   
    net_compressed = GeneNetwork(n_genes_c, perturbation, f_vars_r_m, funcs_c)
    SSD_compressed = net_compressed.get_steady_state()
    
    Difference = np.subtract(expected_SSD, SSD_compressed)  
   
    AbsoluteDifference = abs(Difference)
 
    SumOfAbsoluteDifference = np.sum(AbsoluteDifference)

    NormalizedSumOfAbsoluteDifference = SumOfAbsoluteDifference/(2**n_genes_c)

    NormalizedSumOfAbsoluteDifference = f'{NormalizedSumOfAbsoluteDifference:.10f}'

    with open('comprehensive testing.csv', 'a') as f_object:
        writer_object = writer(f_object)
        writer_object.writerow([NormalizedSumOfAbsoluteDifference])
        f_object.close()

    print('\n', "Normalized Sum of Absolute Difference:", '\n', NormalizedSumOfAbsoluteDifference)

    ############################################
    # filename = f"compressed_{counter}.csv"

     # counter += 1

    # np.savetxt(filename, SSD_compressed, fmt='%.4e', delimiter=',')
    ##################################


    funcs_c = np.delete(funcs_c, [0], axis=1)
    n = n+1

# # ###################################

# # # graph repsentation of the primary network

G_network = nx.DiGraph()
edges_network = [(g2, g1) for g1 in range(n_genes) for g2 in net.f_vars[g1] if g2 > -1]
G_network.add_edges_from(edges_network)
fig_network = plt.figure(figsize=(10,8))
nx.draw_networkx(G_network, with_labels=True)
fig_network.savefig("prim_network.png")

# # graph representation of the state transition diagram of the primary network

# # G_states = nx.DiGraph()
# # edges_states = [(utils.dec_to_bin(g1, n_genes), utils.get_next_state(utils.dec_to_bin(g1, n_genes), 
# # net.f_vars, net.funcs)) 
# # for g1 in range(net.n_states)]
# # G_states.add_edges_from(edges_states)
# # fig_states = plt.figure(figsize=(10,8))
# # nx.draw_networkx(G_states, with_labels=True)
# # fig_states.savefig("prim_network_states.png")

# graph representation of the compressed network

G_network = nx.DiGraph()
edges_network = [(g2, g1) for g1 in range(n_genes_c) for g2 in net_compressed.f_vars[g1] if g2 > -1]
G_network.add_edges_from(edges_network)
fig_network = plt.figure(figsize=(10,8))
nx.draw_networkx(G_network, with_labels=True)
fig_network.savefig("compressed_network.png")