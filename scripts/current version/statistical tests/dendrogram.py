import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.cluster import hierarchy
from openpyxl import load_workbook

# Прочетете данните от .xlsx файл
file_path = 'BasinRatio.xlsx'  # Подмянете 'your_dataset.xlsx' с вашето име на файла
wb = load_workbook(filename=file_path)
sheet = wb.active
data = sheet.values
cols = next(data)

# Създайте DataFrame от данните
data_df = pd.DataFrame(data, columns=cols)

# Извлечете данните от DataFrame и ги конвертирайте в numpy масив
data_array = data_df.values

# Използвайте йерархичното кластериране от scipy
linkage_matrix = hierarchy.linkage(data_array.T, method='complete')

DendrogramType = "BasinRatio"
# Изобразете дендрограмата
plt.figure(figsize=(10, 6))  # Настройте размера на фигурата според вашите нужди
dn = hierarchy.dendrogram(linkage_matrix, labels=cols)
plt.xlabel('Експеримент')
plt.ylabel('Дистанция')
plt.title(DendrogramType)

# Запишете дендрограмата като файл
output_path = DendrogramType  # Подмянете 'dendrogram.png' с желаното име на файл
plt.savefig(output_path)

# Покажете дендрограмата
plt.show()