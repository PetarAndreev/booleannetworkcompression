import pandas as pd
from scipy.stats import kruskal

import scipy.stats as stats
import matplotlib.pyplot as plt
import seaborn as sns

from itertools import combinations
from scipy.stats import mannwhitneyu

# четене на данните от .xlsx файл
df = pd.read_excel('nHamCPrank_logical_only3.xlsx', engine='openpyxl')

# извличане на данните от отделните колони
col1 = df['Ld'].values
col2 = df['Sd'].values
col3 = df['Sc'].values



# извършване на теста на Kruskal-Wallis
stat, p = kruskal(col1, col2, col3)

# извеждане на резултата
print(f"Статистическа стойност: {stat}")
print(f"p-стойност: {p}")

# изобразяване на box plot-a
plt.figure(figsize=(8,6))
sns.boxplot(data=df)
plt.title('Box plot на nHamCPrank')
plt.show()
plt.savefig('BoxPlot.png')


groups = df.columns.tolist()
comb = combinations(groups, 2)

results = []
for pair in comb:
    group1, group2 = pair
    stat, p = mannwhitneyu(df[group1], df[group2])
    diff = "има" if p < 0.05 else "няма"
    results.append([group1, group2, stat, p, diff])

columns = ["Група 1", "Група 2", "Стат. стойност", "p-стойност", "Има стат. разлика"]
df_results = pd.DataFrame(results, columns=columns)
print(df_results)
