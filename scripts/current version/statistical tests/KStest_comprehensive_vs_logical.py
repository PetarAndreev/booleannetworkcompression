import pandas as pd
from scipy.stats import ks_2samp
import matplotlib.pyplot as plt

# Зареждане на данните от първия Excel файл
df1 = pd.read_excel('nSAD.xlsx')

# Зареждане на данните от втория Excel файл
df2 = pd.read_excel('nSAD_logical.xlsx')

# Списък със шестте двойки колони
column_pairs = [('Ld', 'Ld_logical'), ('Lc', 'Lc_logical'), ('Cd', 'Cd_logical'), ('Cc', 'Cc_logical'), ('Sd', 'Sd_logical'), ('Sc', 'Sc_logical')]

results = []  # Списък за съхранение на резултатите

# Извършване на теста на Колмогоров-Смирнов и визуализация на бокс плот за всяка двойка колони
for pair in column_pairs:
    col1, col2 = pair
    data1 = df1[col1].dropna()  # Изключване на редовете с липсващи стойности
    data2 = df2[col2].dropna()
    statistic, p_value = ks_2samp(data1, data2)
    
    
    if p_value < 0.05:  # Проверка за статистическа значимост при ниво на значимост 0.05
        conclusion = "Има статистически значима разлика"
    else:
        conclusion = "Няма статистически значима разлика"
    
    results.append({'Колона 1': col1, 'Колона 2': col2, 'Статистика': statistic, 'p-стойност': p_value, 'Заключение': conclusion})

    # Изчертаване на бокс плот диаграма
    plt.figure()
    plt.boxplot([data1, data2])
    plt.xticks([1, 2], [col1, col2])
    plt.title(f'Бокс плот за {col1} vs. {col2}')
    plt.xlabel('Колона')
    plt.ylabel('Стойности')
    
    # Запазване на бокс плот диаграмата в JPEG файл
    plt.savefig(f'{col1}_vs_{col2}.jpg')

# Създаване на DataFrame от резултатите
results_df = pd.DataFrame(results)

# Запазване на таблицата с резултатите в Excel файл
results_df.to_excel('резултати.xlsx', index=False)

# Извеждане на таблицата с резултатите
print(results_df)

