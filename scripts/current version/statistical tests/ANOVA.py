import pandas as pd
from scipy.stats import f_oneway
import seaborn as sns
import matplotlib.pyplot as plt

# прочитане на файла и преобразуване в Pandas DataFrame
data = pd.read_excel("ANOVA.xlsx")

# извършване на ANOVA анализ
f_statistic, p_value = f_oneway(data['Линейна дал.'], data['Линейна бл.'], data['Кръг дал.'], data['Кръг бл.'], data['Звезда дал.'], data['Звезда бл.'])

print("F-статистика:", f_statistic)
print("p-стойност:", p_value)

# построяване на боксплот
sns.boxplot(data=data)

# запазване на боксплота на файл в PNG формат
plt.savefig("boxplot_ANOVA.png")