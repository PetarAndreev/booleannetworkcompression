import pandas as pd
from scipy import stats

# Зареждане на данните от първия Excel файл
df1 = pd.read_excel('nSAD.xlsx')

# Зареждане на данните от втория Excel файл
df2 = pd.read_excel('nSAD_logical.xlsx')


# Извличане на първата колона от df1 и df2
column1_df1 = df1['Ld']
column1_df2 = df2['Ld_logical']

# Тестване на колмогоров-смирнов за първата колона
ks_statistic1, p_value1 = stats.ks_2samp(column1_df1, column1_df2)
print(f"Колона 1 - K-S статистика: {ks_statistic1}, p-стойност: {p_value1}")

# Извличане на втората колона от df1 и df2
column2_df1 = df1['Lc']
column2_df2 = df2['Lc_logical']

print(column2_df1)
print(column2_df2)

# Тестване на колмогоров-смирнов за втората колона
ks_statistic2, p_value2 = stats.ks_2samp(column2_df1, column2_df2)
print(f"Колона 2 - K-S статистика: {ks_statistic2}, p-стойност: {p_value2}")

# Повторение за останалите колони (Cd, Cc, Sd, Sc)
# ...