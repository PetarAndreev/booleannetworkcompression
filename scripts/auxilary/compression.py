from boolean_networks.steady_state import get_steady_state
from boolean_networks import utils, canalizing
from boolean_networks.gene_network import GeneNetwork
import matplotlib.pyplot as plt
from boolean_networks.MFPT import swap_MFPT, calculate_gamma
from boolean_networks import bn

import numpy as np
from numpy import genfromtxt
from numpy import savetxt
import math, random, pickle, pandas, json
import networkx as nx
import pprint

# URS-1 Setup primary network, provide as input the number of desired genes

n_genes = 4

## FS-1.1 Option 1 - manual input of parameters

# f_vars = np.array ([[1,2,0,3],
#                     [1,3,-1,-1],
#                     [0,-1,-1,-1],
#                     [0,-1,-1,-1]])

# funcs = np.array([[1,0,1,0],
#                   [0,0,0,1],
#                   [0,0,-1,-1],
#                   [0,1,-1,-1],
#                   [0,-1,-1,-1],
#                   [0,-1,-1,-1],
#                   [0,-1,-1,-1],
#                   [0,-1,-1,-1],
#                   [1,-1,-1,-1],
#                   [0,-1,-1,-1],
#                   [0,-1,-1,-1],
#                   [0,-1,-1,-1],
#                   [1,-1,-1,-1],
#                   [0,-1,-1,-1],
#                   [0,-1,-1,-1],
#                   [0,-1,-1,-1]])                                                               

# f_vars = np.array([[0,4],
#                    [0,2],
#                    [3,-1],
#                    [3,4],
#                    [1,-1]])

# funcs = np.array([[0,1,1,1,0],
#                   [0,0,0,0,1],
#                   [0,0,-1,0,-1],
#                   [1,1,-1,0,-1]])  

# network for calibration of SumOfSquaredDifference method
# f_vars = np.array([[2, -1],
#                    [1, -1],
#                    [2, 3],
#                    [0, 1]])

# funcs = np.array([[0,0,0,1],
#                   [1,0,1,0],
#                   [-1,-1,0,1],
#                   [-1,-1,0,1]]) 


## FS-1.1 Option 2 - generate random network

max_vars = (round(math.sqrt(n_genes)))
bns = [None]

n_vars = random.choices(range(1, max_vars+1), k=n_genes)
bns = bn.generate_bn(n_genes, n_vars)

f_vars = bns.f_vars
funcs = bns.funcs

print('\n', "Predictor set of initial network:", '\n', f_vars)
print('\n', "Functions of initial network:", '\n', funcs)

# generate primary network

net = GeneNetwork(n_genes, 0.01, f_vars, funcs)

## FS-1.2 Select genes to be compressed (merged)

# refer to a gene through it`s index starting at zero
x = 0
y = 3
# !!!x should always be smaller than y!!!

# URS-2 Create The application creates a graph representation 
# of the primary network and its state transition diagram

# graph repsentation of the network

# G_network = nx.DiGraph()
# edges_network = [(g2, g1) for g1 in range(n_genes) for g2 in net.f_vars[g1] if g2 > -1]
# G_network.add_edges_from(edges_network)
# fig_network = plt.figure(figsize=(10,8))
# nx.draw_networkx(G_network, with_labels=True)
# fig_network.savefig("prim_network.png")

# graph representation of the state transition diagram of the network

# G_states = nx.DiGraph()
# edges_states = [(utils.dec_to_bin(g1, n_genes), utils.get_next_state(utils.dec_to_bin(g1, n_genes), 
# net.f_vars, net.funcs)) 
# for g1 in range(net.n_states)]
# G_states.add_edges_from(edges_states)
# fig_states = plt.figure(figsize=(10,8))
# nx.draw_networkx(G_states, with_labels=True)
# fig_states.savefig("prim_network_states.png")

# interface to the application created by Svetomir

# def export_to_json(net):
#     attractors = net.get_attractors()
#     edges = net.get_edges()

#     d = [{"state":int(index),
#     "level":int(attractors[index,1]),
#     "attractor":int(attractors[index,0]),
#     "target":int(edges[index])} for index in range(net.n_states)]
    
#     return(json.dumps(d))

# json_object = export_to_json(net)
# with open("../notebooks/test.json", "w") as outfile:
#     outfile.write(json_object)


# URS-3 Calculate tha actual SSD of the primary network

SSD_primary = net.get_steady_state()
# print('\n', "SSD of the primary network:", '\n',SSD_primary)
np.savetxt('Steady_primary.csv', SSD_primary, delimiter=',')

# URS-4 calculate expected SSD of the compressed networks

b_indexes = np.zeros(shape=(0, 1))
for i in range(len(SSD_primary)):
    b_indexes = np.append(b_indexes, utils.dec_to_bin(i, n_genes))
SSD_primary_b = np.stack ((b_indexes, SSD_primary), axis=1)
# print(SSD_primary_b)

undoubtful_00 = [j for i, j in SSD_primary_b if i[x]=='0' and i[y]=='0']
# print(undoubtful_00)
undoubtful_11 = [j for i, j in SSD_primary_b if i[x]=='1' and i[y]=='1']
doubtful_01 = [j for i, j in SSD_primary_b if i[x]=='0' and i[y]=='1']
doubtful_10 = [j for i, j in SSD_primary_b if i[x]=='1' and i[y]=='0']


undoubtful = undoubtful_00 + undoubtful_11
doubtful_01eq0and10eq1 = doubtful_01 + doubtful_10
doubtful_01eq1and10eq0 = doubtful_10 + doubtful_01

undoubtful_f = [float(x) for x in undoubtful]
doubtful_01eq0and10eq1_f = [float(x) for x in doubtful_01eq0and10eq1]
doubtful_01eq1and10eq0_f = [float(x) for x in doubtful_01eq1and10eq0]

expected_SSD_1 = [a + b for a, b in zip(undoubtful_f, doubtful_01eq0and10eq1_f)]
expected_SSD_2 = [a + b for a, b in zip(undoubtful_f, doubtful_01eq1and10eq0_f)]

# print('\n', "Expected SSD 1:", '\n',expected_SSD_1)
# print('\n', "Expected SSD 2:", '\n',expected_SSD_2)
np.savetxt('Expected SSD 1.csv', expected_SSD_1, delimiter=',')
np.savetxt('Expected SSD 2.csv', expected_SSD_2, delimiter=',')

# # np.savetxt('Expected_SSD_1.csv', expected_SSD_1, delimiter=',')
# # np.savetxt('Expected_SSD_2.csv', expected_SSD_2, delimiter=',')
# # print(type(expected_SSD_1))


# # URS-5 NOT READY Compress the network

# # f_vars_r - reindexed array with initial predictors sets

# f_vars_r = f_vars.copy()

# # f_vars_r means f_vars "reindexed"

# for i in np.nditer(f_vars_r, op_flags = ['readwrite']):
#     if i == -1:
#         continue
#     elif i < x: 
#         i[...] += 1
#     elif i == x:
#         i[...] = 0
#     elif i > x and i < y:
#         continue
#     elif i == y:
#         i[...] = 0
#     else:
#         i[...] -=1

# # print('\n', f_vars_r)

# # not used - works only for fixed size array
# # for sub in f_vars_r:
# #     zeros_amount = np.count_nonzero(sub == 0)
# #     if zeros_amount >= 2:
# #         if sub[1] == 0:
# #             sub[1] = sub[2]
# #             sub[2] = -1
# #         elif sub[2] == 0:
# #             sub[2] = -1


# # remove duplicated predictors

# # convert your numpy array to a list
# f_vars_r = f_vars_r.tolist()

# # iterate over the list (sub will contain the sublists)
# for sub in f_vars_r:
#     # get the indices of zeros from sub
#     zero_indices = [i for i, n in enumerate(sub) if n == 0]

#     if len(zero_indices) == 2:
#         sub.pop(zero_indices[1])  # remove the second zero
#         sub.append(-1)  # add a -1 to the end

# # convert the list back to a numpy array
# f_vars_r = np.array(f_vars_r)

# # print('\n', "Re-indexed predictor set of primary network:", '\n', f_vars_r)

# def combineRows(matrix, row1, row2):
#     assert(row1 != row2)
#     if row1 > row2:
#         row1, row2 = row2, row1
#     [rows, columns] = matrix.shape
#     newMatrix = np.full([rows-1, 2*columns], -1)

#     for i in range(columns):
#         newMatrix[0][i] = matrix[row1][i]
#         newMatrix[0][i+columns] = matrix[row2][i]

#     for r in range(1, row1+1):
#         for c in range(columns):
#             newMatrix[r][c] = matrix[r-1][c]

#     for r in range(row1+1, row2):
#         for c in range(columns):
#             newMatrix[r][c] = matrix[r][c]

#     for r in range(row2, rows-1):
#         for c in range(columns):
#             newMatrix[r][c] = matrix[r+1][c]

#     return newMatrix


# def fixIndices(matrix, row1, row2):
#     [rows, columns] = matrix.shape
#     for r in range(rows):
#         for c in range(columns):
#             if matrix[r][c] == row1 or matrix[r][c] == row2:
#                 matrix[r][c] = 0
#             elif matrix[r][c] != -1 and matrix[r][c] < row1:
#                 matrix[r][c] += 1
#             elif matrix[r][c] > row2:
#                 matrix[r][c] -= 1


# def removeDuplicates(row):
#     for i in range(len(row)-1):
#         if row[i] != -1:
#             for j in range(i+1, len(row)):
#                 if row[i] == row[j]:
#                     row[j] = -1


# def reorderRow(row):
#     last = -1
#     for i in range(len(row)):
#         if row[i] != -1:
#             last = i

#     for i in range(last, 0, -1):
#         if row[i] == -1:
#             for j in range(i, last):
#                 row[j] = row[j+1]
#             row[last] = -1
#             last -= 1


# def removeDuplicatesAndReorder(matrix):
#     [rows, columns] = matrix.shape
#     for r in range(rows):
#         removeDuplicates(matrix[r])
#         reorderRow(matrix[r])


# # f_vars_r_m is f_vars_r where two of the predictor sets are merged

# f_vars_r_m = combineRows(f_vars_r, x, y)
# # print(f_vars_r.shape, f_vars_r_m.shape)

# # print('\n', f_vars_r_m)

# # fixIndices(f_vars_r_m, x, y)
# # print('\n', f_vars_r_m)

# removeDuplicatesAndReorder(f_vars_r_m)
# # print('\n', "Predictor set of compressed network:", '\n', f_vars_r_m)

# ######## delete and assign random function
# # delete rows (functions) for the merged genes
# # funcs_c is funcs with deleted merged genes
# funcs_c = np.delete(funcs, [x, y], axis=1)


# random_function = (random.choices(range(0, 2), k=4))
# random_function = np.asarray(random_function)


# funcs_c = np.column_stack((random_function, funcs_c))
# print('\n', "Functions of compressed network (random" '\n', "function assigned to the metagene):", '\n',funcs_c)
###########################
# calibration function
# funcs_c = np.array([[0,0,1],
#                     [0,1,0],
#                     [0,0,1],
#                     [0,0,1]]) 

# print('\n', "Functions of compressed network:", '\n', funcs_c)

# reduce number of genes with one 
# (two deleted genes and one new metagene placed instead of them at position 0)

# n_genes_c = n_genes - 1

# net_compressed = GeneNetwork(n_genes_c, 0.01, f_vars_r_m, funcs_c)
# SSD_compressed = net_compressed.get_steady_state()
# np.savetxt('Steady_compressed.csv', SSD_compressed, delimiter=',')

# Difference1 = np.subtract(expected_SSD_1, SSD_compressed)  
# Difference2 = np.subtract(expected_SSD_2, SSD_compressed) 


# SquaredDifference1 = np.power(Difference1, 2)
# SquaredDifference2 = np.power(Difference2, 2)

# SumOfSquaredDifference1 = np.sum(SquaredDifference1)
# SumOfSquaredDifference2 = np.sum(SquaredDifference2)

# SumOfSquaredDifference1 = f'{SumOfSquaredDifference1:.8f}'
# SumOfSquaredDifference2 = f'{SumOfSquaredDifference2:.8f}'

# print('\n', "Sum of Squared Difference 1:", '\n', SumOfSquaredDifference1)
# print('\n', "Sum of Squared Difference 2:", '\n', SumOfSquaredDifference2)

##### below not used yet

# permutation = [5, 4, 3, 2, 1, 0]
# idx = np.empty_like(permutation)
# idx[permutation] = np.arange(len(permutation))
# f_vars_r_r = f_vars_r[idx, :]
# # f_vars_r_r means f_vars_r "rearranged"
# print(f_vars_r_r)