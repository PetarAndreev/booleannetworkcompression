from boolean_networks.steady_state import get_steady_state
from boolean_networks import utils, canalizing
from boolean_networks.gene_network import GeneNetwork
import matplotlib.pyplot as plt
from boolean_networks.MFPT import swap_MFPT, calculate_gamma
from boolean_networks import bn

import numpy as np
from numpy import genfromtxt
from numpy import savetxt
import numpy.ma as ma
import math, random, pickle, pandas, json
import networkx as nx
import pprint

from csv import writer


# URS-1 Setup primary network, provide as input the number of desired genes

n_genes = 10

f_vars = np.array([[3, 4, -1, -1], 
                   [7, 8,  9, -1], 
                   [2, 3,  9, -1], 
                   [0, 1,  2, -1], 
                   [0, 2, -1, -1],
                   [0, 1,  2, -1],
                   [0, 1,  3,  4],
                   [3, 4,  5, -1],
                   [6, 7, -1, -1],
                   [6, 7, -1, -1]])


funcs = np.array ([[ 0,  1,  0,  0,  0,  0,  0,  0,  0,  0],
                   [ 1,  0,  0,  1,  1,  1,  0,  0,  0,  1],
                   [ 1,  0,  0,  1,  0,  1,  0,  1,  0,  1],
                   [ 0,  1,  0,  1,  0,  0,  1,  0,  1,  0],
                   [-1,  0,  0,  1, -1,  1,  1,  1, -1, -1],
                   [-1,  0,  0,  1, -1,  1,  1,  1, -1, -1],
                   [-1,  0,  0,  1, -1,  1,  1,  1, -1, -1],
                   [-1,  0,  1,  1, -1,  1,  1,  1, -1, -1],
                   [-1, -1, -1, -1, -1, -1,  0, -1, -1, -1],
                   [-1, -1, -1, -1, -1, -1,  0, -1, -1, -1],
                   [-1, -1, -1, -1, -1, -1,  0, -1, -1, -1],
                   [-1, -1, -1, -1, -1, -1,  1, -1, -1, -1],
                   [-1, -1, -1, -1, -1, -1,  0, -1, -1, -1],
                   [-1, -1, -1, -1, -1, -1,  0, -1, -1, -1],
                   [-1, -1, -1, -1, -1, -1,  0, -1, -1, -1],
                   [-1, -1, -1, -1, -1, -1,  1, -1, -1, -1]])

# def xor(a, b):
#     return (a and (not b)) or (b and (not a))

# f_vars = [(3, 4), (7, 8, 9), (2, 3, 9), (0, 1, 2), (0, 2), (0, 1, 2), (0, 1, 3, 4), (3, 4, 5), (6, 7), (6, 7)]
# funcs = utils.apply_boolean_func([2, 3, 3, 3, 2, 3, 4, 3, 2, 2],
#                          [lambda args: xor(args[0], args[1]),
#                           lambda args: xor((not args[1]), args[2]) and (not args[0]),
#                           lambda args: args[0] and args[1] and args[2],
#                           lambda args: args[0] or args[1] or args[2],
#                           lambda args: (not args[0]) and args[1],
#                           lambda args: args[0] or xor(args[1], args[2]),
#                           lambda args: ((not args[0]) and args[1]) or (args[2] and args[3]),
#                           lambda args: args[0] or (args[1] and (not args[2])),
#                           lambda args: args[0] and args[1],
#                           lambda args: xor(args[0], args[1])])

# print('\n', "Predictor set of initial network:", '\n', f_vars)
# print('\n', "Functions of initial network:", '\n', funcs)

net = GeneNetwork(n_genes, 0.01, f_vars, funcs)
SSD_primary = net.get_steady_state()

# select genes to be compressed
x = 0
y = 7

# calculate expected SSD of the compressed networks
b_indexes = np.zeros(shape=(0, 1))
for i in range(len(SSD_primary)):
    b_indexes = np.append(b_indexes, utils.dec_to_bin(i, n_genes))
SSD_primary_b = np.stack ((b_indexes, SSD_primary), axis=1)

undoubtful_00 = [j for i, j in SSD_primary_b if i[x]=='0' and i[y]=='0']
undoubtful_11 = [j for i, j in SSD_primary_b if i[x]=='1' and i[y]=='1']
doubtful_01 = [j for i, j in SSD_primary_b if i[x]=='0' and i[y]=='1']
doubtful_10 = [j for i, j in SSD_primary_b if i[x]=='1' and i[y]=='0']

undoubtful = undoubtful_00 + undoubtful_11
doubtful_01eq0and10eq1 = doubtful_01 + doubtful_10
doubtful_01eq1and10eq0 = doubtful_10 + doubtful_01

undoubtful_f = [float(x) for x in undoubtful]
doubtful_01eq0and10eq1_f = [float(x) for x in doubtful_01eq0and10eq1]
doubtful_01eq1and10eq0_f = [float(x) for x in doubtful_01eq1and10eq0]

expected_SSD_1 = [a + b for a, b in zip(undoubtful_f, doubtful_01eq0and10eq1_f)]
expected_SSD_2 = [a + b for a, b in zip(undoubtful_f, doubtful_01eq1and10eq0_f)]

#### calculate the CP

CP_primary = net.get_canalizing_power([0,1,2,3,4,5,6,7,8,9], range(10), 4, net.get_steady_state())
print('\n', "CP primary:", '\n', CP_primary)

# # # Compress the network
f_vars_r = f_vars.copy()

for i in np.nditer(f_vars_r, op_flags = ['readwrite']):
    if i == -1:
        continue
    elif i < x: 
        i[...] += 1
    elif i == x:
        i[...] = 0
    elif i > x and i < y:
        continue
    elif i == y:
        i[...] = 0
    else:
        i[...] -=1

# remove duplicated predictors
# convert your numpy array to a list
f_vars_r = f_vars_r.tolist()
# iterate over the list (sub will contain the sublists)
for sub in f_vars_r:
    # get the indices of zeros from sub
    zero_indices = [i for i, n in enumerate(sub) if n == 0]
    if len(zero_indices) == 2:
        print('\n', "Execution terminated due to double connection to the merged gene", '\n')
        raise SystemExit
        ### the next two rows to be activated after we have a concept of what to do with the function
        # sub.pop(zero_indices[1])  # remove the second zero
        # sub.append(-1)  # add a -1 to the end


# convert the list back to a numpy array
f_vars_r = np.array(f_vars_r)

# # print('\n', "Re-indexed predictor set of primary network:", '\n', f_vars_r)

def combineRows(matrix, row1, row2):
    assert(row1 != row2)
    if row1 > row2:
        row1, row2 = row2, row1
    [rows, columns] = matrix.shape
    newMatrix = np.full([rows-1, 2*columns], -1)

    for i in range(columns):
        newMatrix[0][i] = matrix[row1][i]
        newMatrix[0][i+columns] = matrix[row2][i]

    for r in range(1, row1+1):
        for c in range(columns):
            newMatrix[r][c] = matrix[r-1][c]

    for r in range(row1+1, row2):
        for c in range(columns):
            newMatrix[r][c] = matrix[r][c]

    for r in range(row2, rows-1):
        for c in range(columns):
            newMatrix[r][c] = matrix[r+1][c]

    return newMatrix

def fixIndices(matrix, row1, row2):
    [rows, columns] = matrix.shape
    for r in range(rows):
        for c in range(columns):
            if matrix[r][c] == row1 or matrix[r][c] == row2:
                matrix[r][c] = 0
            elif matrix[r][c] != -1 and matrix[r][c] < row1:
                matrix[r][c] += 1
            elif matrix[r][c] > row2:
                matrix[r][c] -= 1

def removeDuplicates(row):
    for i in range(len(row)-1):
        if row[i] != -1:
            for j in range(i+1, len(row)):
                if row[i] == row[j]:
                    row[j] = -1

def reorderRow(row):
    last = -1
    for i in range(len(row)):
        if row[i] != -1:
            last = i

    for i in range(last, 0, -1):
        if row[i] == -1:
            for j in range(i, last):
                row[j] = row[j+1]
            row[last] = -1
            last -= 1

def removeDuplicatesAndReorder(matrix):
    [rows, columns] = matrix.shape
    for r in range(rows):
        removeDuplicates(matrix[r])
        reorderRow(matrix[r])

f_vars_r_m = combineRows(f_vars_r, x, y)

# ########## ask Armianov what is this#####
# # # fixIndices(f_vars_r_m, x, y)

removeDuplicatesAndReorder(f_vars_r_m)
# print('\n', "Predictor set of compressed network:", '\n', f_vars_r_m)

# ######## delete and assign all possible functions
# # delete rows (functions) for the merged genes
# # funcs_c is funcs with deleted merged genes
funcs_c = np.delete(funcs, [x, y], axis=1)

### merged function manually set

merged_function = np.array([0, 0, 1, 1, 1, 1, 1, 1, -1, -1, -1, -1, -1, -1, -1, -1])

n_genes_c = n_genes - 1

funcs_c = np.column_stack((merged_function, funcs_c))
# print('\n', "Functions of compressed network (random" '\n', "function assigned to the metagene):", '\n',funcs_c)

net_compressed = GeneNetwork(n_genes_c, 0.01, f_vars_r_m, funcs_c)

CP_compressed = net_compressed.get_canalizing_power([0,1,2,3,4,5,6,7,8], range(9), 4, net_compressed.get_steady_state())
print('\n', "CP compressed:", '\n', CP_compressed)

SSD_compressed = net_compressed.get_steady_state()
    
Difference1 = np.subtract(expected_SSD_1, SSD_compressed)  
Difference2 = np.subtract(expected_SSD_2, SSD_compressed) 

SquaredDifference1 = np.power(Difference1, 2)
SquaredDifference2 = np.power(Difference2, 2)

SumOfSquaredDifference1 = np.sum(SquaredDifference1)
SumOfSquaredDifference2 = np.sum(SquaredDifference2)

NormalizedSumOfSquaredDifference1 = SumOfSquaredDifference1/(2**n_genes_c)
NormalizedSumOfSquaredDifference2 = SumOfSquaredDifference2/(2**n_genes_c)

NormalizedSumOfSquaredDifference1 = f'{NormalizedSumOfSquaredDifference1:.10f}'
NormalizedSumOfSquaredDifference2 = f'{NormalizedSumOfSquaredDifference2:.10f}'

print('\n', "Normalized Sum of Squared Difference 1:", '\n', NormalizedSumOfSquaredDifference1)
print('\n', "Normalized Sum of Squared Difference 2:", '\n', NormalizedSumOfSquaredDifference2)


# ###################################

# # # graph repsentation of the primary network

# # G_network = nx.DiGraph()
# # edges_network = [(g2, g1) for g1 in range(n_genes) for g2 in net.f_vars[g1] if g2 > -1]
# # G_network.add_edges_from(edges_network)
# # fig_network = plt.figure(figsize=(10,8))
# # nx.draw_networkx(G_network, with_labels=True)
# # fig_network.savefig("prim_network.png")

# # graph representation of the state transition diagram of the primary network

# # G_states = nx.DiGraph()
# # edges_states = [(utils.dec_to_bin(g1, n_genes), utils.get_next_state(utils.dec_to_bin(g1, n_genes), 
# # net.f_vars, net.funcs)) 
# # for g1 in range(net.n_states)]
# # G_states.add_edges_from(edges_states)
# # fig_states = plt.figure(figsize=(10,8))
# # nx.draw_networkx(G_states, with_labels=True)
# # fig_states.savefig("prim_network_states.png")

# # f_vars=f_vars_r_m

# # G_network = nx.DiGraph()
# # edges_network = [(g2, g1) for g1 in range(n_genes_c) for g2 in net_compressed.f_vars[g1] if g2 > -1]
# # G_network.add_edges_from(edges_network)
# # fig_network = plt.figure(figsize=(10,8))
# # nx.draw_networkx(G_network, with_labels=True)
# # fig_network.savefig("compressed_network.png")

# # print('\n', "Predictor set of compressed network:", '\n', f_vars)
