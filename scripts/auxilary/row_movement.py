import numpy as np


def combineRows(matrix, row1, row2):
    assert(row1 != row2)
    if row1 > row2:
        row1, row2 = row2, row1
    [rows, columns] = matrix.shape
    newMatrix = np.full([rows-1, 2*columns], -1)

    for i in range(columns):
        newMatrix[0][i] = matrix[row1][i]
        newMatrix[0][i+columns] = matrix[row2][i]

    for r in range(1, row1+1):
        for c in range(columns):
            newMatrix[r][c] = matrix[r-1][c]

    for r in range(row1+1, row2):
        for c in range(columns):
            newMatrix[r][c] = matrix[r][c]

    for r in range(row2, rows-1):
        for c in range(columns):
            newMatrix[r][c] = matrix[r+1][c]

    return newMatrix


def fixIndices(matrix, row1, row2):
    [rows, columns] = matrix.shape
    for r in range(rows):
        for c in range(columns):
            if matrix[r][c] == row1 or matrix[r][c] == row2:
                matrix[r][c] = 0
            elif matrix[r][c] != -1 and matrix[r][c] < row1:
                matrix[r][c] += 1
            elif matrix[r][c] > row2:
                matrix[r][c] -= 1


def removeDuplicates(row):
    for i in range(len(row)-1):
        if row[i] != -1:
            for j in range(i+1, len(row)):
                if row[i] == row[j]:
                    row[j] = -1


def reorderRow(row):
    last = -1
    for i in range(len(row)):
        if row[i] != -1:
            last = i

    for i in range(last, 0, -1):
        if row[i] == -1:
            for j in range(i, last):
                row[j] = row[j+1]
            row[last] = -1
            last -= 1


def removeDuplicatesAndReorder(matrix):
    [rows, columns] = matrix.shape
    for r in range(rows):
        removeDuplicates(matrix[r])
        reorderRow(matrix[r])


#A test case follows...
testMatrix = np.array([
    [3,  5,  6],
    [4, -1, -1],
    [5,  7, -1],
    [1,  6, -1],
    [1,  0,  6],
    [3,  4,  8],
    [2,  3,  5],
    [2, -1, -1],
    [0,  4,  5],
    [0,  5, -1]])

result = combineRows(testMatrix, 2, 4)
print(testMatrix.shape, result.shape)
print('\n', result)

fixIndices(result, 2, 4)
print('\n', result)

removeDuplicatesAndReorder(result)
print('\n', result)