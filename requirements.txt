# =============================================================================
# DEPRECATION WARNING:
#
# The file `requirements.txt` does not influence the package dependencies and
# will not be automatically created in the next version of PyScaffold (v4.x).
#
# Please have look at the docs for better alternatives
# (`Dependency Management` section).
# =============================================================================
#
# Add your pinned requirements so that they can be easily installed with:
# pip install -r requirements.txt
# Remember to also add them in setup.cfg but unpinned.
# Example:
# numpy==1.13.3
# scipy==1.0
#

asn1crypto==1.3.0
attrs==19.3.0
backcall==0.1.0
bleach==3.1.4
# Editable Git install with no remote (boolean-networks==0.0.post0.dev1+g2624cc8)
-e f:\diplomna\boolean_networks\src
certifi==2020.4.5.1
cffi==1.14.0
chardet==3.0.4
colorama==0.4.3
cryptography==2.8
cycler==0.10.0
decorator==4.4.2
defusedxml==0.6.0
entrypoints==0.3
et-xmlfile==1.0.1
idna==2.9
importlib-metadata==1.5.0
ipykernel==5.1.4
ipython==7.13.0
ipython-genutils==0.2.0
jdcal==1.4.1
jedi==0.16.0
Jinja2==2.11.1
json5==0.9.4
jsonschema==3.2.0
jupyter-client==6.1.2
jupyter-core==4.6.3
jupyterlab==1.2.6
jupyterlab-server==1.1.0
kiwisolver==1.2.0
MarkupSafe==1.1.1
matplotlib==3.2.1
mistune==0.8.4
mkl-fft==1.0.15
mkl-random==1.1.0
mkl-service==2.3.0
mock==4.0.1
nbconvert==5.6.1
nbformat==5.0.4
notebook==6.0.3
numexpr==2.7.1
numpy==1.18.1
openpyxl==3.0.3
pandas==1.0.3
pandocfilters==1.4.2
parso==0.6.2
pickleshare==0.7.5
prometheus-client==0.7.1
prompt-toolkit==3.0.4
pycparser==2.20
Pygments==2.6.1
pyOpenSSL==19.1.0
pyparsing==2.4.7
pyrsistent==0.16.0
PyScaffold==3.2.3
PySocks==1.7.1
python-dateutil==2.8.1
pytz==2020.1
pywin32==227
pywinpty==0.5.7
pyzmq==18.1.1
requests==2.23.0
scipy==1.4.1
seaborn==0.10.1
Send2Trash==1.5.0
six==1.14.0
tables==3.6.1
terminado==0.8.3
testpath==0.4.4
tornado==6.0.4
traitlets==4.3.3
urllib3==1.25.8
wcwidth==0.1.9
webencodings==0.5.1
win-inet-pton==1.1.0
wincertstore==0.2
zipp==2.2.0
