
from itertools import combinations
from . import utils
import math
import numpy as np

def get_part_JPD(states, sub_states, sub_genes, JPD):
    part_JPD = np.zeros(len(sub_states))

    # calculate probabilities for states in sub_states
    for i in range(len(sub_states)):
        part_JPD[i] = calculate_prob(sub_genes, sub_states[i],
                                     states, JPD)

    return part_JPD


def calculate_prob(sub_genes, value, states, JPD):

   # from each state get only values (0/1) for sub_genes
   print(states)
   for state in states:
       print(state)
   values = ["".join([state[i] for i in sub_genes]) for state in states]
   
   # sum probabilities for states with specific values for sub_genes
   p = sum([JPD[index] for index in range(len(values)) if values[index] == value])
   
   return p


def round_small_number(x):

    # if very small number -> round to 0
    if x < math.pow(10, -10):
        return(0)

    return(x)

def calculate_cod(n_genes, states, pred_states, part_JPD):

    # calculate provbability for target gene = 1 using part_JPD
    p_target = calculate_prob([0], "1", states, part_JPD)

    # calculate optimal error for predicting target without predictors
    error_target = min(p_target, 1 - p_target)

    # if very small number -> 0
    error_target = round_small_number(error_target)

    # calculate optimal error for each possible values of predictors
    errors = np.zeros(len(pred_states))

    for i in range(len(pred_states)):
        p_state = pred_states[i]

        # minimum of target = 0 and target = 1, given predictors p_state
        errors[i] = min(calculate_prob(range(n_genes), "1" + p_state, states, part_JPD),
                        calculate_prob(range(n_genes), "0" + p_state, states, part_JPD))

    # calculate optimal error for predicting target with predictors
    error_pred = round_small_number(errors.sum())

    # calculate CoD
    if error_target == 0 and error_pred == 0:
        CoD = 1
    else:
        CoD = 1 - error_pred / error_target
        CoD = round_small_number(CoD)

    return CoD

def calculate_cod_multi(master_genes, slave_genes, n_predictors, JPD, states):
    
    # get all possible combinations of n predictors
    pr_comb = list(combinations(slave_genes, n_predictors))

    # get the number of combinations
    n_comb = len(pr_comb)

    # get all posible states of outcome and n predictors
    n_states_sub  = int(math.pow(2, n_predictors + 1))
    sub_states = [utils.dec_to_bin(num, n_predictors + 1) for num in range(n_states_sub)]
    
    # get all possible states of the predictors
    pred_states = [utils.dec_to_bin(num, n_predictors) for num in range(n_states_sub//2)]

    #define matrix for the results
    cod = -1*np.ones((len(master_genes), n_comb))

    for i in range(len(master_genes)):
        master = master_genes[i]

        # combine master gene with each combination of predictor genes
        space = [(master,) + c for c in pr_comb if master not in c]

        # calculate CoD for each combination
        for j in range(len(space)):
            # calculate partition JPD for the observed genes (master, predictors)
            part_JPD = get_part_JPD(states, sub_states, space[j], JPD)
            
            # calculate CoD for master gene i and predictor combination j
            cod[i, j] = calculate_cod(n_predictors + 1, sub_states, pred_states, part_JPD)

    return cod

def calculate_rp(genes, master_genes, slave_genes, n_predictors, JPD):
    """Calculates Regulation Power for the master genes, using n predictor genes

    Args:
    genes (list of ints): genes to be used
    master_genes (list of ints): master genes (subset of genes)
    slave_genes (list of ints):  slave genes (subset of genes)
    n_predictors (int): number of predictors to be used for calculating CoD (<= number of slave genes)
    JPD: Joint Probability Distribution to be used

    Returns:
    numpy array: array containing RP for each master gene
    """

    # generate posible combinations of n predictors out of the slave genes
    pr_comb = list(combinations([genes[i] for i in slave_genes], n_predictors))

    # number of combinations
    n_comb = len(pr_comb)

    # number of all states
    n_states = int(math.pow(2, len(genes)))

    # generate all states
    states = [utils.dec_to_bin(num, len(genes)) for num in range(n_states)]

    # number of posible states for n_predictors+1
    n_states_sub  = int(math.pow(2, n_predictors + 1))

    # generate posible states for master gene and predictors
    sub_states = [utils.dec_to_bin(num, n_predictors + 1) for num in range(n_states_sub)]

    # generate posible states for predictors
    pred_states = [utils.dec_to_bin(num, n_predictors) for num in range(n_states_sub//2)]

    # array(placeholder) for mean cod for each master gene
    mean_cod = [None]*len(master_genes)

    # iterate over master genes
    for i in range(len(master_genes)):
        master = master_genes[i]

        # append master gene in front of the posible combinations of predictors 
        space = [(genes[master],) + c for c in pr_comb]

        # calculate CoD for each combination
        cod = [None]*len(space)
        for j in range(len(space)):
            # get partitional JPD for the combination
            part_JPD = get_part_JPD(states, sub_states, space[j], JPD)

            # calculate CoD
            cod[j] = calculate_cod(n_predictors + 1, sub_states, pred_states, part_JPD)

        # calculate mean of the combinations
        mean_cod[i] = sum(cod) / n_comb

    return mean_cod

def calculate_ip(canalizing_gene, master_genes, slave_genes, n_predictors, JPD, states):
    # number of states
    n_states = len(states)

    # indexes of all states with canalizing gene state ON
    on_indexes = [i for i in range(n_states) if states[i][canalizing_gene] == '1']
    
    # indexes of all states with canalizing gene state OFF
    off_indexes = [i for i in range(n_states) if states[i][canalizing_gene] == '0']

    # get ON states
    states_on = [states[i] for i in on_indexes]

    # get OFF states
    states_off = [states[i] for i in off_indexes]

    # get JPD for on states
    JPD_on = [JPD[i] for i in on_indexes]

    # get JPD for off states
    JPD_off = [JPD[i] for i in off_indexes]

    cod_on = calculate_cod_multi(master_genes, slave_genes, n_predictors, JPD_on, states_on)
    cod_off = calculate_cod_multi(master_genes, slave_genes, n_predictors, JPD_off, states_off)

    delta_cod = np.zeros(len(master_genes))

    for i in range(len(master_genes)):
        off = cod_off[i]
        on = cod_on[i]
        delta_cod[i] = np.mean(off[off > -1] - on[on > -1])

    # sum incap power and enhancing power over all master genes
    ip = delta_cod[delta_cod > 0].sum()
    ep = delta_cod[delta_cod < 0].sum()*(-1)

    return (ip, ep)


