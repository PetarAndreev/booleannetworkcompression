import numpy as np
import random, math
from . import utils, gene_network

class BooleanNetwork:
    def __init__(self, n_genes, f_vars, funcs):
        self.n_genes = n_genes
        self.f_vars = f_vars
        self.funcs = funcs


def generate_bn(n_genes, n_vars):
    max_vars = max(n_vars)
    f_vars = -1*np.ones((n_genes, max_vars))
    
    n_values = int(math.pow(2, max_vars))

    funcs = -1*np.ones((n_values, n_genes))

    for i in range(n_genes):
        f_vars[i, range(n_vars[i])] = sorted(random.sample(range(n_genes), n_vars[i]))
        f_vars = f_vars.astype(int)
        n_vals = int(math.pow(2, n_vars[i]))
        f = [random.random() >= 0.5 for i in range(n_vals)]
        while utils.count_bf_params(f) < n_vars[i]:
            f = [random.random() >= 0.5 for i in range(n_vals)]

        funcs[range(n_vals), i] = f
    
    return BooleanNetwork(n_genes, f_vars, funcs)

    
