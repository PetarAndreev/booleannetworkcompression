import numpy as np
import random, math

def get_random_distr(n_random, size, n_time):
    """ Generates random distributions using beta distribuion

    Args:
    n_random: number of distributions to be generated
    size: number of states
    n_time: number of time points

    Returns:
    np array: array with shape(n_random, n_time, size) containing start random distributions
    and placeholders for the other timepoints
    """
    D = np.zeros((n_random, n_time, size))
    for index in range(0, n_random):
        num_states = random.randint(1, size)
        states = random.sample(range(0, size), num_states)
        a = random.randint(1, size)
        b = random.randint(1, size)
        D[index, 0, states] = np.random.beta(a, b, num_states)
        D[index, 0, states] = D[index, 0, states]/D[index, 0, states].sum()
   
    return D

def calculate_timepoints(D, transition):
    """ Get start distribution and transition matrix and use them to calculate JPD in given timepoints

    Args:
    D (np array): array containing n random starting distribution at positions D[i][0].
    transition (np array): transition matrix to be used

    Returns:
    np array: array with the same shape as D with calculated JPD for each timepoint
    """

    n_random, n_time, _ = D.shape
    for index in range(n_random):
        for t in range(1, n_time):
            D[index, t,] = D[index, t-1,] @ transition

    return D


def dec_to_bin(x, n):
    """Convert decimal number to binary with n digits (adds leading zeros)
    """

    binary = bin(x)[2:]
    n_zeros = n - len(binary)

    return "0"*n_zeros + binary

def bin_to_dec(binary):
    """Convert binary number to decimal

    Args:
    binary (string): binary number

    Returns:
    int: decimal number
    """
    decimal, n = 0, len(binary)
    for i in range(n):
        index = n - i - 1
        bit = int(binary[index])
        decimal = decimal + bit * pow(2, i)

    return(decimal)     
      

def invert_state(state, index):
    """Invert bit at index in string containing 0s and 1s.

    Args:
    state (string): binary number
    index (int): position in the string

    Returns:
    string: binary number the same as state but with flipped bit at position index
    """
    bin = list(state)
    
    if bin[index] == '0':
        bin[index] = '1'
    else:
        bin[index] = '0'

    inverted = "".join(bin)

    return inverted

def number_of_different_bits(a, b):
    """Calculates the number of different bits(characters) between two binary numbers(strings)

    Args:
    a (string): binary number
    b (string): binary number

    Returns:
    int: number of differences
    """
    count = 0
    for i in range(len(a)):
        if a[i] != b[i]:
            count = count + 1

    return count

def swap_genes(states, gene1, gene2, T):
    """ Swap (rename) two genes in boolean network states

    Args:
    states (list of strings): list of states of the network
    gene1 (int): position of first gene to be swaped
    gene2 (int): position of the second gene
    T (np array): transition matrix

    Returns:
    np array: the new transition matrix after renaming
    dict: a dictionary with the mapping of the states after raneming
    """

    # find the mapping of the old states into the new states (after renaming)
    d = {}
    for i in range(len(states)):
        state = states[i]
        if state[gene1] != state[gene2]:
            swaped_state = list(state)
            swaped_state[gene1] = state[gene2]
            swaped_state[gene2] = state[gene1]
            dec = bin_to_dec("".join(swaped_state))
            d[i] = dec
        else:
            d[i] = i
        
    # use to remember allready swaped rows
    flags = np.zeros(len(states))
    T2 = np.copy(T)

    # swap the rows in the transition matrix according to the mapping
    for i in range(len(states)):
        if d[i] != i and flags[i] == 0:
            T2[i] = T[d[i]]
            T2[d[i]] = T[i]
            flags[i] = flags[d[i]] = 1

    # use to remember allready swaped columns
    flags = np.zeros(len(states))

    # swap the columns in the transition matrix according to the mapping
    for i in range(len(states)):
        if d[i] != i and flags[i] == 0:
            T2[:,[i, d[i]]] = T2[:,[d[i], i]]
            flags[i] = flags[d[i]] = 1

    return T2, d

def generate_transition_old(n_genes, perturbation):
    """ Generates random transition matrix with given probability for perturbation"""
    
    n_states = int(math.pow(2, n_genes))
    states = [dec_to_bin(num, n_genes) for num in range(n_states)]
    T = np.zeros((n_states, n_states))
    next_state = random.sample(states, n_states)

    for i in range(n_states):
        next_st = next_state[i] 
        for j in range(n_states):
            d = number_of_different_bits(next_st, states[j])
            T[i, j] = math.pow(
                perturbation, d)*math.pow(1-perturbation, n_genes-d)

    return T

def get_next_state(state, f_vars, funcs):
    next_state = ""
    n_genes = len(state)
    for g in range(n_genes):
        f_var = [var for var in f_vars[g] if var >= 0]
        values = "".join([state[i] for i in f_var])
        dec = bin_to_dec(values)
        next_state = next_state + str(int(funcs[dec, g]))

    return next_state

def generate_transition(n_genes, f_vars, funcs, perturbation):
    """ Generates transition matrix by given boolean functions and perturbation"""
    
    n_states = int(math.pow(2, n_genes))
    states = [dec_to_bin(num, n_genes) for num in range(n_states)]
    T = np.zeros((n_states, n_states))
    
    for i in range(n_states):
        next_st = get_next_state(states[i], f_vars, funcs)
        for j in range(n_states):
            d = number_of_different_bits(states[i], states[j])

            p_pert = (math.pow(perturbation, d)*math.pow(1-perturbation, n_genes-d)) * int(d > 0)
            p_dir = (math.pow(1-perturbation, n_genes)) * int(states[j] == next_st)
            T[i, j] = p_pert + p_dir

    return T


def generate_transition2(n_genes, f_vars, funcs, perturbation):
    """ Generates transition matrix by given boolean functions and perturbation"""
    
    n_states = int(math.pow(2, n_genes))
    states = [dec_to_bin(num, n_genes) for num in range(n_states)]
    T = np.zeros((n_states, n_states))

    for i in range(n_states):
        next_st = get_next_state(states[i], f_vars, funcs)
        for j in range(n_states):
            d = number_of_different_bits(next_st, states[j])
            T[i, j] = math.pow(
                perturbation, d)*math.pow(1-perturbation, n_genes-d)

    return T


def remove_char(word, index):
    word = list(word)
    return "".join([word[i] for i in range(len(word)) if i != index])


def apply_boolean_func(max_vars, functions):
    fn = -1*np.ones((int(math.pow(2, max(max_vars))), len(functions)))

    for g in range(len(functions)):
        max_var = max_vars[g]
        for i in range(int(math.pow(2, max_var))):
            binary = dec_to_bin(i, max_var)
            binary = [int(b) for b in list(binary)]
            fn[i, g] = int(functions[g](binary))

    return fn

def count_bf_params(f):
    n = len(f)
    n_genes = int(math.log2(n))
    values = [dec_to_bin(k, n_genes) for k in range(n)]
    count = 0

    for g in range(n_genes):
        f1 = [f[k] for k in range(n) if values[k][g] == '0']
        f2 = [f[k] for k in range(n) if values[k][g] == '1']
        
        if f1 != f2:
            count = count + 1
    
    return count

# def export_to_json(net):
#     attractors = net.get_attractors()
#     edges = net.get_edges()

#     d = [{"state":int(index),
#     "level":int(attractors[index,1]),
#     "attractor":int(attractors[index,0]),
#     "target":int(edges[index])} for index in range(net.n_states)]
    
#     return(json.dumps(d))
            




