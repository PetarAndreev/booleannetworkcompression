import numpy as np
from . import utils

def MFPT(n_genes, states, undes_inds, transition, gamma):
    """Uses MFPT algorithm to find the best control policy for each of the genes in the network.

    Args:
    n_genes (int): number of genes
    states (list of strings): states of the network
    undes_inds (list of ints): indexes of the undesirable states
    transition (np array): transition matrix of the network
    gamma (np array): gamma parameter for each control gene

    Returns:
    numpy array: array with shape (n_genes, n_states) containing
    control policy for each control gene
    """
    
    # get indexes of desired states
    des_inds = [i for i in range(len(states)) if i not in undes_inds]

    # get submatrix of transition matrix - desired states to desired states
    DD = transition[des_inds,][:, des_inds]

    # get submatrix of transition matrix - undesired states to undesired states
    UU = transition[undes_inds,][:, undes_inds]

    # Ku contains mfpt from each desirable state to the set of undes states
    # Kd contains mfpt from each undesirable state to the set of des states
    Ku = np.zeros(len(states))
    Kd = np.zeros(len(states))

    # calculate Ku and Kd
    Ku[des_inds] = np.linalg.lstsq(np.eye(len(des_inds)) - DD,
                                   np.ones(len(des_inds)), rcond=None)[0]
                        
    Kd[undes_inds] = np.linalg.lstsq(np.eye(len(undes_inds)) - UU,
                                     np.ones(len(undes_inds)), rcond=None)[0]

    control = np.zeros((n_genes, len(states)))

    Ku = np.around(Ku, 4)
    Kd = np.around(Kd, 4)

    # consider each gene as control gene
    for g in range(n_genes):
        for u in undes_inds:
            # flip gene g in state u
            inverted = utils.invert_state(states[u], g)

            # compare Kd for u and u with flipped g
            inverted_u = states.index(inverted)
            if Kd[u] - Kd[inverted_u] > gamma[g]:
                control[g, u] = 1

        for d in des_inds:
            # flip gene g in state d
            inverted = utils.invert_state(states[d], g)

            # compare Ku for d and d with flipped g
            inverted_d = states.index(inverted)
            if Ku[inverted_d] - Ku[d] > gamma[g]:
                control[g, d] = 1

    # return control policies for the genes
    return control


def swap_MFPT(n_genes, states, target, transition, gamma):
    """Uses MFPT algorithm to find the best control policy for each of the genes in the network.
    Before applying the algorithm rename target gene to be gene 0 in the network. After renaming
    we have the desired block structure of the transition matrix.

    Args:
    n_genes (int): number of genes
    states (list of strings): states of the network
    target (int): the target gene to be used for defining desirable and undesirable states.
    transition (np array): transition matrix of the network
    gamma (np array): gamma parameter for each control gene

    Returns:
    numpy array: array with shape (n_genes, n_states) containing
    control policy for each control gene
    """

    # swap target gene with gene 0
    # T - the transition matrix after renaming
    # d - mapping of the states after renaming
    if target != 0:
        T, d = utils.swap_genes(states, 0, target, transition)
    else:
        T = transition
        d = {i:i for i in range(len(states))}
    
    # get indexes of undesired states
    undes_inds = [i  for i in range(len(states)) if states[i][0] == '0']

    # use MFPT to get the control policies
    control = MFPT(n_genes, states, undes_inds, T, gamma)

    # reorder rows and columns in the control policies array to return
    #  the order of the genes before renaming
    temp = control[0]
    control[0] = control[target]
    control[target] = temp

    used = np.zeros(len(states))

    for i in range(len(states)):
        if used[i] == 0:
            control[:,[i, d[i]]] = control[:,[d[i], i]]
            used[i] = used[d[i]] = 1
    
    return control


def calculate_gamma(n_genes, states, target, transition, iterations):
    """Calculates upper limit for gamma parameter for each control gene.

    Args:
    n_genes (int): number of genes
    states (list of strings): states of the network
    target (int): the target gene to be used for defining desirable and undesirable states.
    transition (np array): transition matrix of the network
    iterations (int): the number of iterations to be used

    Returns:
    numpy array: array containing upper limit of gamma for each gene in the network
    """
    step = 1
    count = np.zeros(n_genes)
    gamma = np.zeros(n_genes)

    # calculate upper limit for gamma
    while (count < 3).any():
        control = swap_MFPT(n_genes, states, target, transition, gamma)
        for g in range(n_genes):
            if count[g] < 3:
                gamma[g] = gamma[g] + step
                
                if control[g].sum() == 0:
                    count[g] = count[g] + 1
                else:
                    count[g] = 0

    left = np.zeros(n_genes)
    right = gamma - 3*step*np.ones(n_genes)
    
    # make the interval for gamma smaller
    
    for _ in range(iterations):
        mid = (left + right) / 2
        control_mid = swap_MFPT(n_genes, states, target, transition, mid)
        for g in range(n_genes):
            if control_mid[g].sum() == 0:
                right[g] = mid[g]
            else:
                left[g] = mid[g]

    # return upper limit for gamma after n iterations
    return right




    