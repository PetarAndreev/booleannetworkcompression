import numpy as np
import math

def prepare_matrix(Q):
    dimension = Q.shape[0]
    Q = Q - np.identity(dimension)

    # include sum(pi)  = 1 in the matrix
    M = np.vstack((Q.transpose()[:-1], np.ones(dimension)))
    b = np.vstack((np.zeros((dimension - 1, 1)), [1]))
    return M, b


def get_steady_state(Q, iterations = None):
    """
    Obtain the steady state vector as the vector that 
    gives the minimum of a least squares optimisation problem.
    
    - Q: the transition matrix
    """
    # use equation pi*T = pi (pi - steady state, T - transition matrix)
    # => pi(T - I) = 0 and sum(pi) = 1

    M, b = prepare_matrix(Q)
    pi, _, _, _ = np.linalg.lstsq(M, b, rcond=None)
    steady = pi.transpose()[0]

    precision = math.ceil(-math.log10(min(steady))) + 2
    treshold = math.pow(10, -precision)

    prev = None
    count = 0
    i = 0
    # iterate to calculate better aproximation for the steady state distribution
    while count < 100 and (iterations is None or i < iterations):
        prev = steady
        steady = steady @ Q
        steady = steady / sum(steady)
        i = i + 1
        if all(abs(prev - steady) < treshold):
            count = count + 1
        else:
            count = 0
    
    return steady