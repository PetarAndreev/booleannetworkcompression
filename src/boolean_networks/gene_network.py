from itertools import combinations
from . import utils, canalizing, steady_state
import math, random
import numpy as np


class GeneNetwork:
    def __init__(self, n_genes, perturbation, f_vars = None, funcs = None, transition = None):
        # genes with numbers from 0 to n_genes - 1
        self.genes = range(n_genes)

        # boolean functions variables
        self.f_vars = f_vars

        # boolean functions
        self.funcs = funcs

        # probability for perturbaion
        self.perturbation = perturbation

        # number of states in boolean network
        self.n_states = int(math.pow(2, n_genes))

        # states like binary numbers
        self.states = [utils.dec_to_bin(num, n_genes) for num in range(self.n_states)]
        
        # transition matrix
        if transition is None:
            self.transition_matrix = utils.generate_transition(n_genes,
                                    self.f_vars, self.funcs, self.perturbation)
        else:
            self.transition_matrix = transition

        # steady state distribution
        self.steady_state = None

    def get_steady_state(self):
        """Finds and returns steady state distribution of the network

            Returns:
            numpy array: steady state distribution for states in the network
        """
        if self.steady_state is None:
            self.steady_state = steady_state.get_steady_state(self.transition_matrix, self.n_states)

        return self.steady_state
    
    def get_reg_power(self, master_genes, slave_genes, n_predictors, JPD):
        """Calculates Regulation Power for the master genes, using n predictor genes

            Args:
            master_genes ([int]): master genes
            slave_genes ([int]): slave genes
            n_predictors (int): number of the predictors to be used
            JPD: Joint Probability Distribution to be used

            Returns:
            numpy array: array containing RP for each master gene
        """
        # calculate CoD for each combination of target gene and n predictor genes
        cod = canalizing.calculate_cod_multi(master_genes, slave_genes,
                                             n_predictors, JPD, self.states)

        # number of target(master) genes, number of combinations of n predictors
        n_master, n_comb = list(cod.shape)

        # calculate Regulation power for each master gene
        mean_cod = np.zeros(n_master)
        for index in range(n_master):
            c = cod[index,]
            mean_cod[index] = np.mean(c[c > -1])
        
        return mean_cod

    def get_incap_power(self, master_genes, slave_genes, n_predictors, JPD):
        """Calculates Incapacitating Power for the master genes, using n predictor genes
        
            Args:
            master_genes ([int]): master genes
            slave_genes ([int]): slave genes
            n_predictors (int): number of the predictors to be used
            JPD: Joint Probability Distribution to be used

            Returns:
            numpy array: array containing IP for each master gene
        """

        # calculate Incapacitating power for each master gene
        ip = np.zeros(len(master_genes))

        for i in range(len(master_genes)):
            # use master gene i as canalizing gene
            canalizing_gene = master_genes[i]

            # get the other master genes
            masters = [gene for gene in master_genes if gene != canalizing_gene]
            
            # calculate incapacitating power for master gene i
            ip[i] = canalizing.calculate_ip(canalizing_gene, masters, slave_genes,
                                            n_predictors, JPD, self.states)[0]

        return ip

    def get_canalizing_power(self, master_genes, slave_genes, n_predictors, JPD):
        """Calculates Canalizing Power for the master genes, using n predictor genes
        
            Args:
            master_genes ([int]): master genes
            slave_genes ([int]): slave genes
            n_predictors (int): number of the predictors to be used
            JPD: Joint Probability Distribution to be used

            Returns:
            numpy array: array containing CP for each master gene
        """

        # calculate regulation power
        rp = self.get_reg_power(master_genes, slave_genes, n_predictors, JPD)

        # calculate incapacitating power
        ip = self.get_incap_power(master_genes, slave_genes, n_predictors, JPD)

        # calculate canalizing power
        return rp + ip

    def get_random_observations(self, n_random, n_time):
        """Generates n random starting distributions of the states.
           Use them to calculate distributions for n_time timepoints.
        
            Args:
            n_random (int): number of random start distributions
            n_time (int): number of timepoints

            Returns:
            numpy array: array with shape (n_random, n_time, n_states) containing
            generated distributions
        """

        # get random initial beta distributions
        D = utils.get_random_distr(n_random, self.n_states, n_time)

        # calculate distribution for each timepoint
        D = utils.calculate_timepoints(D, self.transition_matrix)

        return D

    def apply_control_policy(self, gene, policy):
        """Applies control policy over given gene, calculating the new transition matrix.
        
            Args:
            gene (int): the control gene
            policy (np array): control policy

            Returns:
            GeneNetwork: boolean network with changed transition matrix according
            to the control policy
        """

        # copy the transition matrix
        T = np.copy(self.transition_matrix)

        # iterate over all states
        for i in range(self.n_states):
            # if control should be applied
            if policy[i] > 0:
                # flip the control gene in the state
                inverted = utils.invert_state(self.states[i], gene)
                
                # calculate the new probabilities in the transition matrix
                for j in range(self.n_states):
                    d = utils.number_of_different_bits(inverted, self.states[j])
                    T[i, j] = math.pow(
                self.perturbation, d)*math.pow(1-self.perturbation, len(self.genes)-d)

        return GeneNetwork(len(self.genes), self.perturbation, None, None, T)

    def get_edges(self):
        edges = -1*np.ones(self.n_states, dtype="int")

        for i in range(self.n_states):
            edges[i] = np.where(self.transition_matrix[i] == np.amax(self.transition_matrix[i]))[0]

        return edges

    def get_next_state(self, start, n):
        while n > 0:
            start = random.choices(range(self.n_states), self.transition_matrix[start], k=1)[0]
            yield start
            n = n - 1

    def get_attractors(self):
        edges = self.get_edges()
        attractors = -1*np.ones((len(edges), 2))
        attr_count = 0

        for start_node in range(len(edges)):
            if attractors[start_node, 0] > -1:
                continue

            node = start_node
            history = []
            while attractors[node, 0] == -1:
                history.append(node)
                attractors[node, 0] = attr_count
                node = edges[node]

            if attractors[node, 0] == attr_count:
                attr_count = attr_count + 1
                attractors[node, 1] = 0
                next_node = edges[node]
                while node != next_node:
                    attractors[next_node, 1] = 0
                    next_node = edges[next_node]
            else:
                attractors[history, 0] = attractors[node, 0]

        for node in range(len(edges)):
            if attractors[node, 1] == -1:
                path = [node]
                next_node = edges[node]
                while attractors[next_node, 1] == -1:
                    path.append(next_node)
                    next_node = edges[next_node]
                count = attractors[next_node, 1] + 1

                for n in reversed(path):
                    attractors[n, 1] = count
                    count = count + 1

        return attractors

    def export_to_json(self):
        attractors = self.get_attractors()
        edges = self.get_edges()

        d = [{"state":int(index),
        "level":int(attractors[index,1]),
        "attractor":int(attractors[index,0]),
        "target":int(edges[index])} for index in range(self.n_states)]
        
        return(json.dumps(d))
            
            










